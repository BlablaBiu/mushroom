package compiler;

import compiler.AST.Basic.BasicType;
import compiler.AST.Declare.ClassDec;
import compiler.AST.Declare.FuncDec;
import compiler.AST.Declare.VarDec;
import compiler.AST.Type.*;
import compiler.Paser.MushroomParser;

/**
 * Created by blacko on 2017/4/3.
 */

public class Built_in {

    public static VarDec var(BasicType tmp) {
        VarDec item = new VarDec();
        item.type = tmp;
        return item;
    }

    public static FuncDec print_() {
        FuncDec item = new FuncDec();
        item.parameters.add(var(new StringType()));
        item.returnType = new VoidType();
        item.ID = "print";
        item.inline = false;
        return item;
    }

    public static FuncDec printInt_() {
        FuncDec item = new FuncDec();
        item.parameters.add(var(new IntType()));
        item.returnType = new VoidType();
        item.ID = "printInt";
        item.inline = false;
        return item;
    }

    public static FuncDec println_() {
        FuncDec item = new FuncDec();
        item.parameters.add(var(new StringType()));
        item.returnType = new VoidType();
        item.ID = "println";
        item.inline = false;
        return item;
    }

    public static FuncDec getString_() {
        FuncDec item = new FuncDec();
        item.returnType = new StringType();
        item.ID = "getString";
        item.inline = false;
        return item;
    }

    public static FuncDec getInt_() {
        FuncDec item = new FuncDec();
        item.returnType = new IntType();
        item.ID = "getInt";
        item.inline = false;
        return item;
    }

    public static FuncDec toString_() {
        FuncDec item = new FuncDec();
        item.parameters.add(var(new IntType()));
        item.returnType = new StringType();
        item.ID = "toString";
        item.inline = false;
        return item;
    }

    public static FuncDec arraySize() {
        FuncDec item = new FuncDec();
        item.parameters.add(var(new ArrayType()));
        item.returnType = new IntType();
        item.ID = "Array_size";
        item.inline = false;
        return item;
    }

    public static FuncDec stringLength() {
        FuncDec item = new FuncDec();
        item.parameters.add(var(new StringType()));
        item.returnType = new IntType();
        item.ID = "String_length";
        item.inline = false;
        return item;
    }

    public static FuncDec stringSubString() {
        FuncDec item = new FuncDec();
        item.parameters.add(var(new StringType()));
        item.parameters.add(var(new IntType()));
        item.parameters.add(var(new IntType()));
        item.returnType = new StringType();
        item.ID = "String_substring";
        item.inline = false;
        return item;
    }

    public static FuncDec stringParseInt() {
        FuncDec item = new FuncDec();
        item.parameters.add(var(new StringType()));
        item.returnType = new IntType();
        item.ID = "String_parseInt";
        item.inline = false;
        return item;
    }

    public static FuncDec stringord() {
        FuncDec item = new FuncDec();
        item.parameters.add(var(new StringType()));
        item.parameters.add(var(new IntType()));
        item.returnType = new IntType();
        item.ID = "String_ord";
        item.inline = false;
        return item;
    }

    public static FuncDec stringAdd() {
        FuncDec item = new FuncDec();
        item.parameters.add(var(new StringType()));
        item.parameters.add(var(new StringType()));
        item.returnType = new StringType();
        item.ID = "String_add";
        item.inline = false;
        return item;
    }

    public static FuncDec stringEQ() {
        FuncDec item = new FuncDec();
        item.parameters.add(var(new StringType()));
        item.parameters.add(var(new StringType()));
        item.returnType = new BoolType();
        item.ID = "String_eq";
        item.inline = false;
        return item;
    }
    public static FuncDec stringNE() {
        FuncDec item = new FuncDec();
        item.parameters.add(var(new StringType()));
        item.parameters.add(var(new StringType()));
        item.returnType = new BoolType();
        item.ID = "String_ne";
        item.inline = false;
        return item;
    }
    public static FuncDec stringLT() {
        FuncDec item = new FuncDec();
        item.parameters.add(var(new StringType()));
        item.parameters.add(var(new StringType()));
        item.returnType = new BoolType();
        item.ID = "String_lt";
        item.inline = false;
        return item;
    }
    public static FuncDec stringGT() {
        FuncDec item = new FuncDec();
        item.parameters.add(var(new StringType()));
        item.parameters.add(var(new StringType()));
        item.returnType = new BoolType();
        item.ID = "String_gt";
        item.inline = false;
        return item;
    }
    public static FuncDec stringLE() {
        FuncDec item = new FuncDec();
        item.parameters.add(var(new StringType()));
        item.parameters.add(var(new StringType()));
        item.returnType = new BoolType();
        item.ID = "String_le";
        item.inline = false;
        return item;
    }
    public static FuncDec stringGE() {
        FuncDec item = new FuncDec();
        item.parameters.add(var(new StringType()));
        item.parameters.add(var(new StringType()));
        item.returnType = new BoolType();
        item.ID = "String_ge";
        item.inline = false;
        return item;
    }
}
