package compiler;

import com.sun.org.apache.xerces.internal.impl.dv.ValidatedInfo;
import com.sun.org.apache.xpath.internal.operations.Bool;
import compiler.IR.Function;
import compiler.IR.IR;
import compiler.IR.IRInstrution.*;
import compiler.IR.Operand.ImmediateNumber;
import compiler.IR.Operand.Register.Register;

import java.util.*;

/**
 * Created by blacko on 2017/5/30.
 */
public class RegisterAllocator {
    IR root;
    List<IrInstruction> exList;

    RegisterAllocator(IR _root) {
        root = _root;
        for(Function item : root.functions) solve(item);
    }
    IR niceShoot() {
        return root;
    }

    void mergeLabel(Function x) {
        List<IrInstruction> newIrSet = new ArrayList<>();
        Set<Label> useLabel = new HashSet<>();
        HashMap<Label, Label> labelMap = new HashMap<>();
        for(IrInstruction item : x.insList) {
            if(item instanceof Label) {
                labelMap.put((Label)item, (Label)item);
            }
            if(item instanceof Jump) useLabel.add(((Jump) item).label);
            if(item instanceof CmpAndJump) useLabel.add(((CmpAndJump) item).target);
        }
        IrInstruction last = null;
        for(int i = 0; i < x.insList.size(); ++i){
            IrInstruction item = x.insList.get(i);
            if(item instanceof Label && !useLabel.contains(item)) continue;
            if(last instanceof Jump) {
                if(!(item instanceof Label)) continue;
            }
            if(last instanceof Label) {
                if(item instanceof Label) {
                    labelMap.put((Label) item,(Label) last);
                    continue;
                }
            }
            if(last != null) newIrSet.add(last);
            last = item;
        }
        if(last != null) newIrSet.add(last);
        for(IrInstruction item : newIrSet) {
            if(item instanceof Jump) {
                Label tmp = ((Jump) item).label;
                for(Label next = labelMap.get(tmp); next != tmp; tmp = next, next = labelMap.get(tmp)) ;
                ((Jump) item).label = tmp;
            }
            if(item instanceof CmpAndJump) {
                Label tmp = ((CmpAndJump) item).target;
                for(Label next = labelMap.get(tmp); next != tmp; tmp = next, next = labelMap.get(tmp));
                ((CmpAndJump) item).target = tmp;
            }
        }
        x.insList = newIrSet;
    }

    void mergeIf(Function x) {
        List<IrInstruction> newSet = new ArrayList<>();
        for(int i = 1; i < x.insList.size(); ++i) {
            boolean Doit = false;
            if(x.insList.get(i - 1) instanceof BinaryOperation && x.insList.get(i) instanceof CmpAndJump) {
                BinaryOperation up =(BinaryOperation )x.insList.get(i - 1);
                CmpAndJump down = (CmpAndJump) x.insList.get(i);
                if(up.dest == down.lhs && down.rhs instanceof ImmediateNumber) {
                    if(((ImmediateNumber) down.rhs).value != 1) continue;
                    String newOp = "";
                    switch (up.op) {
                        case "==": newOp = "je"; break;
                        case "!=": newOp = "jne"; break;
                        case ">=": newOp = "jge"; break;
                        case "<=": newOp = "jle"; break;
                        case ">" : newOp = "jg"; break;
                        case "<" : newOp = "jl"; break;
                        default:
                            throw new RuntimeException("???");
                    }
                    if(!newOp.equals("")){
                        Doit = true;
                        i += 1;
                        newSet.add(new CmpAndJump(up.lhs, up.rhs, newOp, down.target));
                    }
                }
            }
            if(Doit) continue;
            newSet.add(x.insList.get(i - 1));
            if(i + 1 == x.insList.size()) newSet.add(x.insList.get(i));
        }
        x.insList = newSet;
    }

    void build(Function x) {
        exList = new ArrayList<>();
        IrInstruction last = null;
        for(IrInstruction item : x.insList) {
            if(item instanceof Call) {
                IrInstruction ilast = last;
                for(IrInstruction iitem : ((Call) item).callIr) {
                    exList.add(iitem);
                    if(ilast != null) ilast.next = iitem;
                    ilast = iitem;
                }
                last = ilast;
                continue;
            }
            exList.add(item);
            if(item instanceof Jump) {
                ((Jump) item).label.prev.add(item);
                if(last != null) last.next = item;
                last = null;
                continue;
            }
            if(item instanceof CmpAndJump) ((CmpAndJump) item).target.prev.add(item);
            if(last != null) last.next = item;
            last = item;
        }
    }

    private boolean merge(Set<Register> in, Set<Register> out, Register def) {
        boolean doit = false;
        for(Register item : out) {
            if(item != def && !in.contains(item)) {
                in.add(item);
                doit = true;
            }
        }
        return doit;
    }

    private void color(Register def, Set<Register> out, boolean[][] map) {
        if(def == null) return;
        for(Register item : out) {
            map[def.index][item.index] = true;
            map[item.index][def.index] = true;
        }
    }
    private void allocate(Function x) {
        build(x);
        InAndOut tmp = new InAndOut();
        tmp.start = true;
        for(IrInstruction item : x.insList) tmp.visit(item);
        boolean doit = true;
        int times = 0;
        while(doit) {
            doit = false;
            ++times;
            for (int i = exList.size() - 1; i >= 0; --i) {
                IrInstruction item = exList.get(i);
                if (item instanceof Call) throw new RuntimeException("???");
                if (item instanceof Jump) {
                    doit |= merge(item.in, ((Jump) item).label.in, item.def);
                    continue;
                }
                if (item instanceof CmpAndJump) {
                    doit |= merge(item.in, ((CmpAndJump) item).target.in, item.def);
                }
                if (item.next != null) {
                    doit |= merge(item.in, item.next.in, item.def);
                }
            }
        }
        boolean[][] colorMap = new boolean[x.numOfVirtualReg][x.numOfVirtualReg];
        for (IrInstruction item : exList) {
//            if(item.next != null && item instanceof Move && ((Move) item).dest instanceof Register && ((Move) item).source instanceof Register) {
//                for(Register iitem : item.next.in) {
//                    if(iitem != ((Move) item).source)
//                    colorMap[item.def.index][iitem.index] = true;
//                    colorMap[iitem.index][item.def.index] = true;
//                }
//                continue;
//            }
            if (item instanceof Jump) {
                color(item.def, ((Jump) item).label.in, colorMap);
                continue;
            }
            if (item instanceof CmpAndJump) {
                color(item.def, ((CmpAndJump) item).target.in, colorMap);
            }
            if (item.next != null) {
                color(item.def, item.next.in, colorMap);
            }
        }
        int[] setColor = new int[x.numOfVirtualReg];
        int[] useColor = new int[x.numOfVirtualReg];
        boolean[] done = new boolean[x.numOfVirtualReg];
        for(int i = 0; i < x.numOfVirtualReg; ++i) {
            setColor[i] = useColor[i] = 0;
            done[i] = false;
        }
        times = 0;
        for(int i = 0; i < 16; ++i) setColor[i] = i;
        for(int i = 16; i < x.numOfVirtualReg; ++i) setColor[i] = -1;
        x.useRegTag = new boolean[16];
        for(int i = 0; i < 16; ++i) x.useRegTag[i] = false;
        for(int i = 16; i < x.numOfVirtualReg; ++i) {
            // ---
            int tmpi = i;
//            int Maxcnt = -1;
//            for(int k = 16; k < x.numOfVirtualReg; ++k) {
//                if(setColor[k] != -1) continue;
//                ++times;
//                for(int j = 0; j < x.numOfVirtualReg; ++j) {
//                    if(j != k && colorMap[k][j] && setColor[j] != -1) {
//                        useColor[setColor[j]] = times;
//                    }
//                }
//                int cnt = 0;
//                for(int j = 0; j < x.numOfVirtualReg; ++j) {
//                    if(useColor[j] == times) ++cnt;
//                }
//                if(cnt >= Maxcnt) {
//                    Maxcnt = cnt;
//                    tmpi = k;
//                }
//            }
            // --
            ++times;
            for(int j = 0; j < x.numOfVirtualReg; ++j) {
                if(colorMap[tmpi][j] && setColor[j] != -1) {
                    useColor[setColor[j]] = times;
                }
            }
            for(int j = 0; j <= x.numOfVirtualReg; j = (j == 10 ? 16 : j + 1)) {
                int tmpj = 0;
                if (j < 16 && x.name.equals("main")) {
                    tmpj = 10 - j;
                } else {
                    tmpj = j;
                }
                if(useColor[tmpj] != times) {
                    setColor[tmpi] = tmpj;
                    x.regList.get(tmpi).index = tmpj;
                    if(tmpj < 16) x.useRegTag[tmpj] = true;
                    break;
                }
            }
        }
    }

    private void solve(Function x) {
        if(x.numOfVirtualReg > 400) return;
        x.optim = true;
        mergeLabel(x);
        mergeIf(x);
        allocate(x);
    }
}
