package compiler.AST;

import compiler.AST.Basic.*;
import compiler.AST.Declare.ClassDec;
import compiler.AST.Declare.FuncDec;
import compiler.AST.Declare.VarDec;
import compiler.AST.Expression.*;
import compiler.AST.Program;
import compiler.AST.Statment.*;
import compiler.AST.Type.*;

/**
 * Created by blacko on 2017/4/3.
 */
public interface AstVisitor {
    void visit(BasicNode x);
    void visit(Program x);

    void visit(BasicDec x);
    void visit(BasicStat x);
    void visit(BasicExpr x);
    void visit(BasicType x);

    void visit(ClassDec x);
    void visit(FuncDec x);
    void visit(VarDec x);

    void visit(ArrayExpr x);
    void visit(AssignmentExpr x);
    void visit(BinaryExpr x);
    void visit(ConstantExpr x);
    void visit(FuncExpr x);
    void visit(MemberExpr x);
    void visit(NewArrayExpr x);
    void visit(NewExpr x);
    void visit(PostfixExpr x);
    void visit(UnaryExpr x);
    void visit(VarExpr x);

    void visit(BlockStat x);
    void visit(BreakStat x);
    void visit(ContinueStat x);
    void visit(ExprStat x);
    void visit(ForStat x);
    void visit(IfStat x);
    void visit(ReturnStat x);

    void visit(ArrayType x);
    void visit(BoolType x);
    void visit(ClassType x);
    void visit(IntType x);
    void visit(StringType x);
    void visit(VoidType x);
}