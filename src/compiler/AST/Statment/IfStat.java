package compiler.AST.Statment;

import compiler.AST.Basic.BasicExpr;
import compiler.AST.Basic.BasicStat;
import compiler.AST.AstVisitor;
import compiler.AST.Declare.VarDec;
import compiler.IR.Operand.Operand;

import java.util.HashMap;

/**
 * Created by blacko on 2017/3/29.
 */
public class IfStat extends BasicStat{
    public BasicExpr expr;
    public BasicStat trueStat, falseStat;

    @Override
    public void visit(AstVisitor x) {x.visit(this);}
    public String toString(String taps) {
        String x = taps + "if (" + expr.toString("") + ")\n";
        x += trueStat.toString(taps + "\t");
        if(falseStat != null) x += taps + "\n" + taps + "else\n";
        if(falseStat != null) x += falseStat.toString(taps + "\t");
        return x;
    }
    public BasicStat Inline(HashMap<VarDec, Operand> NewMap) {
        IfStat newStat = new IfStat();
        newStat.expr = expr.Inline(NewMap);
        newStat.trueStat = trueStat.Inline(NewMap);
        if(falseStat != null)
            newStat.falseStat = falseStat.Inline(NewMap);
        return  newStat;
    }
}
