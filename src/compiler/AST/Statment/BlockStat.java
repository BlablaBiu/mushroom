package compiler.AST.Statment;

import compiler.AST.Basic.BasicNode;
import compiler.AST.Basic.BasicStat;
import compiler.AST.Declare.VarDec;
import compiler.IR.Operand.Operand;
import compiler.LinkMap;
import compiler.AST.AstVisitor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by blacko on 2017/3/29.
 */
public class BlockStat extends BasicStat{
    public List<BasicNode> VarDecAndStat = new ArrayList<>();
    public LinkMap map;
    @Override
    public void visit(AstVisitor x) {x.visit(this);}
    public String toString(String taps) {
        String x = taps.substring(0, taps.length() - 1) + "{\n";
        for(BasicNode item : VarDecAndStat)
            x += item.toString(taps) + '\n';
        x += taps.substring(0, taps.length() - 1)  + "}";
        return x;
    }
    public BasicStat Inline(HashMap<VarDec, Operand> NewMap) {
        BlockStat newStat = new BlockStat();
        newStat.VarDecAndStat = new ArrayList<>();
        for(BasicNode item : VarDecAndStat){
            if(item instanceof VarDec)
                newStat.VarDecAndStat.add(((VarDec) item).Inline(NewMap));
            else
                newStat.VarDecAndStat.add(((BasicStat)item).Inline(NewMap));
        }
        return newStat;
    }

}
