package compiler.AST.Statment;

import compiler.AST.Basic.BasicExpr;
import compiler.AST.Basic.BasicStat;
import compiler.AST.Declare.FuncDec;
import compiler.AST.AstVisitor;
import compiler.AST.Declare.VarDec;
import compiler.IR.Operand.Operand;

import java.util.HashMap;

/**
 * Created by blacko on 2017/3/29.
 */
public class ReturnStat extends BasicStat{
    public FuncDec upperFunc;
    public BasicExpr expr;

    @Override
    public void visit(AstVisitor x) {x.visit(this);}
    public String toString(String taps) {
        String x = taps + "return ";
        if(expr != null) x += expr.toString("");
        return x;
    }

    public BasicStat Inline(HashMap<VarDec, Operand> NewMap) {
        ReturnStat newStat = new ReturnStat();
        newStat.expr = expr.Inline(NewMap);
        return newStat;
    }
}
