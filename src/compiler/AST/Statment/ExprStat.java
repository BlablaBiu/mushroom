package compiler.AST.Statment;

import compiler.AST.Basic.BasicExpr;
import compiler.AST.Basic.BasicStat;
import compiler.AST.AstVisitor;
import compiler.AST.Declare.VarDec;
import compiler.IR.Operand.Operand;

import java.util.HashMap;

/**
 * Created by blacko on 2017/3/29.
 */
public class ExprStat extends BasicStat{
    public BasicExpr expr;

    @Override
    public void visit(AstVisitor x) {x.visit(this);}

    public String toString(String taps) {
        if(expr == null) return taps;
        return taps + expr.toString("");
    }

    public BasicStat Inline(HashMap<VarDec, Operand> NewMap) {
        ExprStat newStat = new ExprStat();
        newStat.expr = expr.Inline(NewMap);
        return  newStat;
    }
}
