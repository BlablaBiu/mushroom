package compiler.AST.Statment;

import compiler.AST.Basic.BasicStat;
import compiler.AST.AstVisitor;
import compiler.AST.Declare.VarDec;
import compiler.IR.Operand.Operand;

import java.util.HashMap;

/**
 * Created by blacko on 2017/3/29.
 */
public class ContinueStat extends BasicStat{
    public BasicStat upperStat;

    @Override
    public void visit(AstVisitor x) {x.visit(this);}

    public String toString(String taps) {
        return taps + "continue";
    }

    public BasicStat Inline(HashMap<VarDec, Operand> NewMap) {
        return this;
    }
}
