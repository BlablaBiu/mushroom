package compiler.AST.Statment;

import compiler.AST.Basic.BasicExpr;
import compiler.AST.Basic.BasicStat;
import compiler.AST.AstVisitor;
import compiler.AST.Declare.VarDec;
import compiler.IR.Operand.Operand;

import java.util.HashMap;

/**
 * Created by blacko on 2017/3/29.
 */
public class ForStat extends BasicStat{
    public BasicExpr init, cond, step;
    public BasicStat stat;

    @Override
    public void visit(AstVisitor x) {x.visit(this);}

    public String toString(String taps) {
        String x = taps + "for (";
        if(init != null) x += init.toString("");
        x += ", ";
        if(cond != null) x += cond.toString("");
        x += ", ";
        if(step != null) x += step.toString("");
        x += ")\n";
        x += stat.toString(taps + "\t");
        return x;
    }
    public BasicStat Inline(HashMap<VarDec, Operand> NewMap) {
        ForStat newStat = new ForStat();
        if(init != null) newStat.init = init.Inline(NewMap);
        if(cond != null) newStat.cond = cond.Inline(NewMap);
        if(step != null) newStat.step = step.Inline(NewMap);
        newStat.stat = stat.Inline(NewMap);
        return newStat;
    }
}
