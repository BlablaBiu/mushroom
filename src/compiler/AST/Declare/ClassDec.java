package compiler.AST.Declare;

import compiler.AST.Basic.BasicDec;
import compiler.AST.Expression.FuncExpr;
import compiler.LinkMap;
import compiler.AST.AstVisitor;

import java.util.ArrayList;
import java.util.List;
/**
 * Created by blacko on 2017/3/29.
 */
public class ClassDec extends BasicDec {
    public String ID;
    public LinkMap map;
    public List<BasicDec> decs = new ArrayList<>();
    public int size = 0; // IR
    public FuncDec startFunc = null;
    @Override
    public void visit(AstVisitor x) {x.visit(this);}
    public  String toString(String taps) {
        String x = taps + "class " + ID + " {\n";
        for(BasicDec item : decs)
            if (item instanceof VarDec) x += taps + item.toString(taps + '\t') + "\n";

        for(BasicDec item : decs)
            if (item instanceof FuncDec) x += taps + item.toString(taps + '\t') + "\n";

        x += taps + "}";
        return x;
    }
}
