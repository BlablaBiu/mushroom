package compiler.AST.Declare;

import compiler.AST.Basic.*;
import compiler.AST.AstVisitor;
import compiler.AST.Statment.BlockStat;
import compiler.IR.Operand.Operand;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by blacko on 2017/3/29.
 */
public class VarDec extends BasicDec {
    public BasicType type;
    public String ID;
    public BasicExpr expr = null ;
    public ClassDec upperClass = null;
    public boolean isParameter = false;
    public boolean isGlobal = false; // IR
    public boolean isClassMember = false; // IR
    public Operand position = null;
    public int offset = 0; // for class offset
//    public boolean isParameter = false; // IR
    @Override
    public void visit(AstVisitor x) {x.visit(this);}

    public  String toString(String taps) {
        String x = taps + type.toString("") + " " + ID;
        if(expr != null) {
            x += " = ";
            x += expr.toString("");
        }
        if(upperClass != null) {
            x += " [upclass : " + upperClass.ID + "]";
        }
        return x;
    }

    public VarDec Inline(HashMap<VarDec, Operand> NewMap) {
        VarDec newDec = new VarDec();
        newDec.type = type;
        newDec.ID = "inline__" + ID;
        if(expr != null) newDec.expr = expr.Inline(NewMap);
        return newDec;
    }
}
