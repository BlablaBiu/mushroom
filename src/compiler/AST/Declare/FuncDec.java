package compiler.AST.Declare;

import compiler.AST.Basic.BasicDec;
import compiler.AST.Basic.BasicType;
import compiler.AST.AstVisitor;
import compiler.AST.Statment.BlockStat;
import compiler.IR.IRInstrution.Label;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by blacko on 2017/3/29.
 */
public class FuncDec extends BasicDec {
    public BasicType returnType;
    public ClassDec upperClass;
    public String ID;
    public List<VarDec> parameters = new ArrayList<>();
    public BlockStat stat;
    public Label label; // for IR
    public boolean inline = true;
    //----------let BlockStat declare for Func
    public boolean isDec;
    //----------
    @Override
    public void visit(AstVisitor x) {x.visit(this);}
    public  String toString(String taps) {
        String x = taps;
        if(returnType == null) {
            x += "null";
        } else {
            x += returnType.toString("");
        }
        x += " " + ID + "(";
        for(int i = 0; i < parameters.size(); ++i) {
            if(i != 0) {
                x += ", ";
            }
            x += parameters.get(i).toString("");
        }
        x += ")";
        if(upperClass != null) {
            x += " [upclass : " + upperClass.ID + "]";
        }
        x += "\n";
        x += stat.toString(taps + '\t');
        return x;
    }
}
