package compiler.AST.Expression;

import compiler.AST.Basic.BasicExpr;
import compiler.AST.AstVisitor;
import compiler.AST.Declare.VarDec;
import compiler.IR.Operand.Operand;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by blacko on 2017/4/1.
 */
public class NewExpr extends BasicExpr{

    @Override
    public void visit(AstVisitor x) {x.visit(this);}

    public String toString(String taps) {
        return "new " + type.toString("");
    }

    public NewExpr(NewExpr other) {
        super(other);
    }
    public NewExpr(){}
    public NewExpr Inline(HashMap<VarDec, Operand> newMap){
        return new NewExpr(this);
    }
}
