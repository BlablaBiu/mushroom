package compiler.AST.Expression;

import compiler.AST.Basic.BasicExpr;
import compiler.AST.Declare.FuncDec;
import compiler.AST.AstVisitor;
import compiler.AST.Declare.VarDec;
import compiler.IR.Operand.Operand;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by blacko on 2017/4/1.
 */
public class FuncExpr extends BasicExpr{
    public FuncDec funcDec;
    public List<BasicExpr> parameters = new ArrayList<>();

    @Override
    public void visit(AstVisitor x) {x.visit(this);}

    public FuncExpr(){}
    public String toString(String taps) {
        String tem = funcDec.ID;
        tem += "(";
        for(int i = 0; i < parameters.size(); ++i) {
            if(i != 0) {
                tem += ", ";
            }
            tem += parameters.get(i).toString("");
        }
        tem += ")";
        return  tem;
    }
    public FuncExpr(FuncExpr other) {
        super(other);
    }
    public FuncExpr Inline(HashMap<VarDec, Operand> newMap){
        FuncExpr newExpr = new FuncExpr(this);
        newExpr.funcDec =funcDec;
        newExpr.parameters = new ArrayList<>();
        for(BasicExpr item: parameters) {
            newExpr.parameters.add(item.Inline(newMap));
        }
        return newExpr;
    }
}

