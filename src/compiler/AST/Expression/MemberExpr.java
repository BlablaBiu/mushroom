package compiler.AST.Expression;

import compiler.AST.Basic.BasicExpr;
import compiler.AST.Declare.VarDec;
import compiler.AST.AstVisitor;
import compiler.IR.Operand.Operand;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by blacko on 2017/4/1.
 */
public class MemberExpr extends BasicExpr {
    public BasicExpr expr;
    public String ID;
    public VarDec varDec;
    @Override
    public void visit(AstVisitor x) {x.visit(this);}
    public String toString(String taps) {
        return expr.toString("") + "." + ID;
    }

    public MemberExpr(MemberExpr other) {
        super(other);
    }

    public MemberExpr(){}
    public MemberExpr Inline(HashMap<VarDec, Operand> newMap){
        MemberExpr newExpr = new MemberExpr(this);
        newExpr.expr = expr.Inline(newMap);
        newExpr.ID = ID;
        newExpr.varDec = varDec;
        return newExpr;
    }
}
