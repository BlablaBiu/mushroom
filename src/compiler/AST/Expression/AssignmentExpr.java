package compiler.AST.Expression;

import compiler.AST.Basic.BasicExpr;
import compiler.AST.AstVisitor;
import compiler.AST.Declare.VarDec;
import compiler.IR.Operand.Operand;

import java.util.HashMap;

/**
 * Created by blacko on 2017/4/1.
 */
public class AssignmentExpr extends BasicExpr {
    public BasicExpr lExpr, rExpr;

    @Override
    public void visit(AstVisitor x) {x.visit(this);}

    public String toString(String taps) {
        return lExpr.toString("") + " = " + rExpr.toString("");
    }

    public AssignmentExpr(AssignmentExpr other) {
        super(other);
    }
    public AssignmentExpr(){}
    public BasicExpr Inline(HashMap<VarDec, Operand> newMap){
        AssignmentExpr newExpr = new AssignmentExpr(this);
        newExpr.lExpr = lExpr.Inline(newMap);
        newExpr.rExpr = rExpr.Inline(newMap);
        return newExpr;
    }
}
