package compiler.AST.Expression;

import compiler.AST.Basic.BasicExpr;
import compiler.AST.AstVisitor;
import compiler.AST.Declare.VarDec;
import compiler.IR.Operand.Operand;

import java.util.HashMap;
import java.util.Optional;

/**
 * Created by blacko on 2017/4/1.
 */
public class BinaryExpr extends BasicExpr{
    public String op;
    public BasicExpr lExpr, rExpr;

    @Override
    public void visit(AstVisitor x) {x.visit(this);}
    public String toString(String taps) {
        return "(" + lExpr.toString("") + " " + op + " " + rExpr.toString("") + ")";
    }
    public BinaryExpr(BinaryExpr other) {
        super(other);
    }
    public BinaryExpr(){}
    public BasicExpr Inline(HashMap<VarDec, Operand> newMap){
        BinaryExpr newExpr = new BinaryExpr(this);
        newExpr.lExpr = lExpr.Inline(newMap);
        newExpr.op = op;
        newExpr.rExpr = rExpr.Inline(newMap);
        return newExpr;
    }
}

