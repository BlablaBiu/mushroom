package compiler.AST.Expression;

import compiler.AST.Basic.BasicExpr;
import compiler.AST.Basic.BasicType;
import compiler.AST.AstVisitor;
import compiler.AST.Declare.VarDec;
import compiler.IR.Operand.Operand;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by blacko on 2017/4/2.
 */
public class NewArrayExpr extends BasicExpr{
    public BasicType bodyType;
    public List<BasicExpr> expDim = new ArrayList<>();
    public int emptyDim;


    @Override
    public void visit(AstVisitor x) {x.visit(this);}

    public String toString(String taps) {
        String tem = "new " + bodyType.toString("");
        for (BasicExpr i : expDim) {
            tem += "[" + i.toString("") + "]";
        }
        for (int i = 0; i < emptyDim; ++i) {
            tem += "[]";
        }
        return tem;
    }
    public NewArrayExpr(){}
    public NewArrayExpr(NewArrayExpr other) {
        super(other);
    }
    public NewArrayExpr Inline(HashMap<VarDec, Operand> newMap){
        NewArrayExpr newExpr = new NewArrayExpr(this);
        newExpr.bodyType = bodyType;
        newExpr.emptyDim = emptyDim;
        newExpr.expDim = new ArrayList<>();
        for(BasicExpr item : expDim)  {
            newExpr.expDim.add(item.Inline(newMap));
        }
        return newExpr;
    }
}
