package compiler.AST.Expression;

import compiler.AST.Basic.BasicExpr;
import compiler.AST.AstVisitor;
import compiler.AST.Declare.VarDec;
import compiler.AST.Type.*;
import compiler.IR.Operand.Operand;

import java.util.HashMap;

public class ConstantExpr extends BasicExpr{
    public int valInt;
    public String valStr;
    public boolean valBool;
    public ConstantExpr(int val) { type = new IntType(); valInt = val; }
    public ConstantExpr(String val) {type = new StringType(); valStr = val; }
    public ConstantExpr(boolean val) {type = new BoolType(); valBool = val; }
    public ConstantExpr() {type = new NullType(); }

    @Override
    public void visit(AstVisitor x) {x.visit(this);}

    public String toString(String taps) {
        if (type instanceof IntType) {
            return Integer.toString(valInt);
        } else if (type instanceof StringType) {
            return valStr;
        } else if (type instanceof BoolType) {
            return Boolean.toString(valBool);
        } else {
            return "NULL";
        }
    }
    public ConstantExpr(ConstantExpr other) {
        super(other);
    }
    public BasicExpr Inline(HashMap<VarDec, Operand> newMap){
        return this;
    }
}
