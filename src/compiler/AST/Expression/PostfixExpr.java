package compiler.AST.Expression;

import compiler.AST.Basic.BasicExpr;
import compiler.AST.AstVisitor;
import compiler.AST.Declare.VarDec;
import compiler.IR.Operand.Operand;
import javafx.geometry.Pos;

import java.util.HashMap;

/**
 * Created by blacko on 2017/4/1.
 */
public class PostfixExpr extends BasicExpr{
    public String op;
    public BasicExpr expr;

    @Override
    public void visit(AstVisitor x) {x.visit(this);}

    public String toString(String taps) {
        return expr.toString("") + op;
    }

    public PostfixExpr(PostfixExpr other) {
        super(other);
    }
    public PostfixExpr(){}
    public PostfixExpr Inline(HashMap<VarDec, Operand> newMap){
        PostfixExpr newExpr = new PostfixExpr(this);
        newExpr.op = op;
        newExpr.expr = expr.Inline(newMap);
        return newExpr;
    }
}
