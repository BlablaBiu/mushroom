package compiler.AST.Expression;

import compiler.AST.Basic.BasicExpr;
import compiler.AST.Declare.VarDec;
import compiler.AST.AstVisitor;
import compiler.IR.Operand.Operand;

import java.util.HashMap;

/**
 * Created by blacko on 2017/4/1.
 */
public class VarExpr extends BasicExpr{
    public VarDec varDec;
    @Override
    public void visit(AstVisitor x) { x.visit(this);}

    public String toString(String taps) {
        return varDec.ID;
    }
    public VarExpr(){}
    public BasicExpr Inline(HashMap<VarDec, Operand> newMap){
        VarExpr newExpr = new VarExpr();
        newExpr.varDec = new VarDec();
        if(newMap.containsKey(varDec)) {
            newExpr.varDec.position = newMap.get(varDec);
            return newExpr;
        }
        return this;
    }
}
