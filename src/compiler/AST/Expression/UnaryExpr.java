package compiler.AST.Expression;

import compiler.AST.Basic.BasicExpr;
import compiler.AST.AstVisitor;
import compiler.AST.Declare.VarDec;
import compiler.IR.Operand.Operand;

import java.util.HashMap;

/**
 * Created by blacko on 2017/4/1.
 */
public class UnaryExpr extends BasicExpr{
    public String op;
    public BasicExpr expr;

    @Override
    public void visit(AstVisitor x) {x.visit(this);}

    public String toString(String taps) {
        return op + expr.toString("");
    }

    public UnaryExpr(UnaryExpr other) {
        super(other);
    }
    public UnaryExpr(){}
    public UnaryExpr Inline(HashMap<VarDec, Operand> newMap){
        UnaryExpr newExpr = new UnaryExpr(this);
        newExpr.op = op;
        newExpr.expr = expr.Inline(newMap);
        return newExpr;
    }
}
