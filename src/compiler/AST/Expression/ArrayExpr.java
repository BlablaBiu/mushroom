package compiler.AST.Expression;

import compiler.AST.Basic.BasicExpr;
import compiler.AST.AstVisitor;
import compiler.AST.Basic.BasicType;
import compiler.AST.Declare.VarDec;
import compiler.AST.Type.ArrayType;
import compiler.IR.Operand.Operand;

import java.util.HashMap;

/**
 * Created by blacko on 2017/4/1.
 */
public class ArrayExpr extends BasicExpr{
    public BasicExpr outExpr, inExpr;

    @Override
    public void visit(AstVisitor x) {x.visit(this);}

    public String toString(String taps) {
        return outExpr.toString("") + "[" + inExpr.toString("") + "]";
    }
    public ArrayExpr(ArrayExpr other) {
        super(other);
    }
    public ArrayExpr(){}
    public BasicExpr Inline(HashMap<VarDec, Operand> newMap){
        ArrayExpr newExpr = new ArrayExpr(this);
        newExpr.outExpr = outExpr.Inline(newMap);
        newExpr.inExpr = inExpr.Inline(newMap);
        return newExpr;
    }
}
