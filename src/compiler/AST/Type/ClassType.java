package compiler.AST.Type;

import compiler.AST.Basic.BasicType;
import compiler.AST.Declare.ClassDec;
import compiler.AST.AstVisitor;

/**
 * Created by blacko on 2017/3/31.
 */
public class ClassType extends BasicType {
    public ClassDec classDec;
    public ClassType(ClassDec classDec_) {
        classDec = classDec_;
    }
    public int size;
    @Override
    public void visit(AstVisitor x) {x.visit(this);}

    @Override
    public boolean accept(BasicType x){
        if(x instanceof ClassType) {
           return classDec == ((ClassType) x).classDec;
        } else {
            return (x instanceof NullType);
        }
    }

    public String toString(String taps) {
        return classDec.ID;
    }
}
