package compiler.AST.Type;

import compiler.AST.Basic.BasicType;
import compiler.AST.Declare.ClassDec;
import compiler.AST.AstVisitor;

/**
 * Created by blacko on 2017/3/31.
 */
public class ArrayType extends BasicType {
    public BasicType bodyType;
    public int dim;
    public static ClassDec classDec;
    public boolean acceptAll = false;

    public int size; // IR
//    public int size() {
//        return ;
//    }
    public ArrayType() {
        acceptAll = true;
    }

    public ArrayType(BasicType bodyType_, int dim_) {
        bodyType = bodyType_;
        dim = dim_;
    }

    @Override
    public void visit(AstVisitor x) {x.visit(this);}

    @Override
    public boolean accept(BasicType x){
        if(x instanceof ArrayType) {
            if(acceptAll) return true;
            return ((((ArrayType) x).bodyType.getClass() == bodyType.getClass()) && ((ArrayType) x).dim == dim);
        } else {
            return (x instanceof NullType);
        }
    }

    public BasicType memberType() {
        if(dim == 1) {
            return bodyType;
        } else {
            return new ArrayType(bodyType, dim - 1);
        }
    }

    public String toString(String taps) {
        String x = bodyType.toString("");
        for (int i = 0; i < dim; ++i) {
            x += "[]";
        }
        return x;
    }

}
