package compiler.AST.Type;

import compiler.AST.Basic.BasicType;
import compiler.AST.AstVisitor;

/**
 * Created by blacko on 2017/4/3.
 */
public class IntType extends BasicType{

    @Override
    public void visit(AstVisitor x) {x.visit(this);}

    @Override
    public boolean accept(BasicType x){
        return (x instanceof IntType);
    }

    public String toString(String taps) {
        return "int";
    }

}
