package compiler.AST.Basic;

import compiler.AST.AstVisitor;

/**
 * Created by blacko on 2017/3/30.
 */
public abstract class BasicNode {
    public BasicNode() {}
    public abstract void visit(AstVisitor x);
    public abstract String toString(String taps);
}
