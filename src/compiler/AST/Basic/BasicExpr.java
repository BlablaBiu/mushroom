package compiler.AST.Basic;


import compiler.AST.Declare.VarDec;
import compiler.IR.Operand.Operand;

import java.util.HashMap;

/**
 * Created by blacko on 2017/3/29.
 */
public abstract class BasicExpr extends BasicNode {
    public BasicType type;
    public boolean isLeftValue = false;
    public Operand operand; //IR
    public abstract BasicExpr Inline(HashMap<VarDec, Operand> NewMap);
    public BasicExpr() {
    }
    public BasicExpr(BasicExpr other) {
        type = other.type;
        isLeftValue = other.isLeftValue;
        operand = null;
    }
}
