package compiler.AST.Basic;

import compiler.AST.Declare.VarDec;
import compiler.IR.Operand.Operand;

import java.util.HashMap;

/**
 * Created by blacko on 2017/3/29.
 */
public abstract class BasicStat extends BasicNode {
    public abstract BasicStat Inline(HashMap<VarDec, Operand> NewMap);
    public BasicStat() {
    }
}
