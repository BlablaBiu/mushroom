package compiler.AST.Basic;

/**
 * Created by blacko on 2017/3/29.
 */
public abstract class BasicType extends BasicNode {
    public abstract boolean accept(BasicType x);
//    public abstract int size();
}
