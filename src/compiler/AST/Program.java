package compiler.AST;

import compiler.AST.Basic.BasicDec;
import compiler.AST.Basic.BasicNode;
import compiler.AST.Declare.ClassDec;
import compiler.AST.Declare.FuncDec;
import compiler.AST.Declare.VarDec;
import compiler.LinkMap;

import java.util.ArrayList;
import java.util.List;
/**
 * Created by blacko on 2017/3/29.
 */
public class Program extends BasicNode {
    public LinkMap map;
    public List<BasicDec> decs = new ArrayList<>();
    public Program() {}

    @Override
    public void visit(AstVisitor x) {x.visit(this);}
    public String toString(String taps) {
        String x = "";
        for(BasicDec item : decs)
            if(item instanceof ClassDec) x += item.toString("") + "\n";
        for(BasicDec item : decs)
            if(item instanceof VarDec) x += item.toString("") + "\n";
        for(BasicDec item : decs)
            if(item instanceof FuncDec) x += item.toString("") + "\n";
        return x;
    }
}
