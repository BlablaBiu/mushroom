// Generated from /Users/blacko/Git/mushroom-compiler/Mushroom/src/compiler/Paser/Mushroom.g4 by ANTLR 4.6
package compiler.Paser;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link MushroomParser}.
 */
public interface MushroomListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link MushroomParser#program}.
	 * @param ctx the parse tree
	 */
	void enterProgram(MushroomParser.ProgramContext ctx);
	/**
	 * Exit a parse tree produced by {@link MushroomParser#program}.
	 * @param ctx the parse tree
	 */
	void exitProgram(MushroomParser.ProgramContext ctx);
	/**
	 * Enter a parse tree produced by {@link MushroomParser#classDec}.
	 * @param ctx the parse tree
	 */
	void enterClassDec(MushroomParser.ClassDecContext ctx);
	/**
	 * Exit a parse tree produced by {@link MushroomParser#classDec}.
	 * @param ctx the parse tree
	 */
	void exitClassDec(MushroomParser.ClassDecContext ctx);
	/**
	 * Enter a parse tree produced by {@link MushroomParser#funcDec}.
	 * @param ctx the parse tree
	 */
	void enterFuncDec(MushroomParser.FuncDecContext ctx);
	/**
	 * Exit a parse tree produced by {@link MushroomParser#funcDec}.
	 * @param ctx the parse tree
	 */
	void exitFuncDec(MushroomParser.FuncDecContext ctx);
	/**
	 * Enter a parse tree produced by the {@code toBlockStat}
	 * labeled alternative in {@link MushroomParser#stat}.
	 * @param ctx the parse tree
	 */
	void enterToBlockStat(MushroomParser.ToBlockStatContext ctx);
	/**
	 * Exit a parse tree produced by the {@code toBlockStat}
	 * labeled alternative in {@link MushroomParser#stat}.
	 * @param ctx the parse tree
	 */
	void exitToBlockStat(MushroomParser.ToBlockStatContext ctx);
	/**
	 * Enter a parse tree produced by the {@code exprStat}
	 * labeled alternative in {@link MushroomParser#stat}.
	 * @param ctx the parse tree
	 */
	void enterExprStat(MushroomParser.ExprStatContext ctx);
	/**
	 * Exit a parse tree produced by the {@code exprStat}
	 * labeled alternative in {@link MushroomParser#stat}.
	 * @param ctx the parse tree
	 */
	void exitExprStat(MushroomParser.ExprStatContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ifStat}
	 * labeled alternative in {@link MushroomParser#stat}.
	 * @param ctx the parse tree
	 */
	void enterIfStat(MushroomParser.IfStatContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ifStat}
	 * labeled alternative in {@link MushroomParser#stat}.
	 * @param ctx the parse tree
	 */
	void exitIfStat(MushroomParser.IfStatContext ctx);
	/**
	 * Enter a parse tree produced by the {@code whileStat}
	 * labeled alternative in {@link MushroomParser#stat}.
	 * @param ctx the parse tree
	 */
	void enterWhileStat(MushroomParser.WhileStatContext ctx);
	/**
	 * Exit a parse tree produced by the {@code whileStat}
	 * labeled alternative in {@link MushroomParser#stat}.
	 * @param ctx the parse tree
	 */
	void exitWhileStat(MushroomParser.WhileStatContext ctx);
	/**
	 * Enter a parse tree produced by the {@code forStat}
	 * labeled alternative in {@link MushroomParser#stat}.
	 * @param ctx the parse tree
	 */
	void enterForStat(MushroomParser.ForStatContext ctx);
	/**
	 * Exit a parse tree produced by the {@code forStat}
	 * labeled alternative in {@link MushroomParser#stat}.
	 * @param ctx the parse tree
	 */
	void exitForStat(MushroomParser.ForStatContext ctx);
	/**
	 * Enter a parse tree produced by the {@code continueStat}
	 * labeled alternative in {@link MushroomParser#stat}.
	 * @param ctx the parse tree
	 */
	void enterContinueStat(MushroomParser.ContinueStatContext ctx);
	/**
	 * Exit a parse tree produced by the {@code continueStat}
	 * labeled alternative in {@link MushroomParser#stat}.
	 * @param ctx the parse tree
	 */
	void exitContinueStat(MushroomParser.ContinueStatContext ctx);
	/**
	 * Enter a parse tree produced by the {@code breakStat}
	 * labeled alternative in {@link MushroomParser#stat}.
	 * @param ctx the parse tree
	 */
	void enterBreakStat(MushroomParser.BreakStatContext ctx);
	/**
	 * Exit a parse tree produced by the {@code breakStat}
	 * labeled alternative in {@link MushroomParser#stat}.
	 * @param ctx the parse tree
	 */
	void exitBreakStat(MushroomParser.BreakStatContext ctx);
	/**
	 * Enter a parse tree produced by the {@code returnStat}
	 * labeled alternative in {@link MushroomParser#stat}.
	 * @param ctx the parse tree
	 */
	void enterReturnStat(MushroomParser.ReturnStatContext ctx);
	/**
	 * Exit a parse tree produced by the {@code returnStat}
	 * labeled alternative in {@link MushroomParser#stat}.
	 * @param ctx the parse tree
	 */
	void exitReturnStat(MushroomParser.ReturnStatContext ctx);
	/**
	 * Enter a parse tree produced by {@link MushroomParser#blockStat}.
	 * @param ctx the parse tree
	 */
	void enterBlockStat(MushroomParser.BlockStatContext ctx);
	/**
	 * Exit a parse tree produced by {@link MushroomParser#blockStat}.
	 * @param ctx the parse tree
	 */
	void exitBlockStat(MushroomParser.BlockStatContext ctx);
	/**
	 * Enter a parse tree produced by {@link MushroomParser#toVarDec}.
	 * @param ctx the parse tree
	 */
	void enterToVarDec(MushroomParser.ToVarDecContext ctx);
	/**
	 * Exit a parse tree produced by {@link MushroomParser#toVarDec}.
	 * @param ctx the parse tree
	 */
	void exitToVarDec(MushroomParser.ToVarDecContext ctx);
	/**
	 * Enter a parse tree produced by {@link MushroomParser#varDec}.
	 * @param ctx the parse tree
	 */
	void enterVarDec(MushroomParser.VarDecContext ctx);
	/**
	 * Exit a parse tree produced by {@link MushroomParser#varDec}.
	 * @param ctx the parse tree
	 */
	void exitVarDec(MushroomParser.VarDecContext ctx);
	/**
	 * Enter a parse tree produced by the {@code assignmentExpr}
	 * labeled alternative in {@link MushroomParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterAssignmentExpr(MushroomParser.AssignmentExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code assignmentExpr}
	 * labeled alternative in {@link MushroomParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitAssignmentExpr(MushroomParser.AssignmentExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code funcExpr}
	 * labeled alternative in {@link MushroomParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterFuncExpr(MushroomParser.FuncExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code funcExpr}
	 * labeled alternative in {@link MushroomParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitFuncExpr(MushroomParser.FuncExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code newNonArrayExpr}
	 * labeled alternative in {@link MushroomParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterNewNonArrayExpr(MushroomParser.NewNonArrayExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code newNonArrayExpr}
	 * labeled alternative in {@link MushroomParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitNewNonArrayExpr(MushroomParser.NewNonArrayExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code arrayExpr}
	 * labeled alternative in {@link MushroomParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterArrayExpr(MushroomParser.ArrayExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code arrayExpr}
	 * labeled alternative in {@link MushroomParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitArrayExpr(MushroomParser.ArrayExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code memberExpr}
	 * labeled alternative in {@link MushroomParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterMemberExpr(MushroomParser.MemberExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code memberExpr}
	 * labeled alternative in {@link MushroomParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitMemberExpr(MushroomParser.MemberExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code binaryExpr}
	 * labeled alternative in {@link MushroomParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterBinaryExpr(MushroomParser.BinaryExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code binaryExpr}
	 * labeled alternative in {@link MushroomParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitBinaryExpr(MushroomParser.BinaryExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code memberFuncExpr}
	 * labeled alternative in {@link MushroomParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterMemberFuncExpr(MushroomParser.MemberFuncExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code memberFuncExpr}
	 * labeled alternative in {@link MushroomParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitMemberFuncExpr(MushroomParser.MemberFuncExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code error}
	 * labeled alternative in {@link MushroomParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterError(MushroomParser.ErrorContext ctx);
	/**
	 * Exit a parse tree produced by the {@code error}
	 * labeled alternative in {@link MushroomParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitError(MushroomParser.ErrorContext ctx);
	/**
	 * Enter a parse tree produced by the {@code subExpr}
	 * labeled alternative in {@link MushroomParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterSubExpr(MushroomParser.SubExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code subExpr}
	 * labeled alternative in {@link MushroomParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitSubExpr(MushroomParser.SubExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code varExpr}
	 * labeled alternative in {@link MushroomParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterVarExpr(MushroomParser.VarExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code varExpr}
	 * labeled alternative in {@link MushroomParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitVarExpr(MushroomParser.VarExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code unaryExpr}
	 * labeled alternative in {@link MushroomParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterUnaryExpr(MushroomParser.UnaryExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code unaryExpr}
	 * labeled alternative in {@link MushroomParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitUnaryExpr(MushroomParser.UnaryExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code postfixExpr}
	 * labeled alternative in {@link MushroomParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterPostfixExpr(MushroomParser.PostfixExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code postfixExpr}
	 * labeled alternative in {@link MushroomParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitPostfixExpr(MushroomParser.PostfixExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code newArrayExpr}
	 * labeled alternative in {@link MushroomParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterNewArrayExpr(MushroomParser.NewArrayExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code newArrayExpr}
	 * labeled alternative in {@link MushroomParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitNewArrayExpr(MushroomParser.NewArrayExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code constantExpr}
	 * labeled alternative in {@link MushroomParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterConstantExpr(MushroomParser.ConstantExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code constantExpr}
	 * labeled alternative in {@link MushroomParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitConstantExpr(MushroomParser.ConstantExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code toNonArrayType}
	 * labeled alternative in {@link MushroomParser#type}.
	 * @param ctx the parse tree
	 */
	void enterToNonArrayType(MushroomParser.ToNonArrayTypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code toNonArrayType}
	 * labeled alternative in {@link MushroomParser#type}.
	 * @param ctx the parse tree
	 */
	void exitToNonArrayType(MushroomParser.ToNonArrayTypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code arrayType}
	 * labeled alternative in {@link MushroomParser#type}.
	 * @param ctx the parse tree
	 */
	void enterArrayType(MushroomParser.ArrayTypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code arrayType}
	 * labeled alternative in {@link MushroomParser#type}.
	 * @param ctx the parse tree
	 */
	void exitArrayType(MushroomParser.ArrayTypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code voidType}
	 * labeled alternative in {@link MushroomParser#nonArrayType}.
	 * @param ctx the parse tree
	 */
	void enterVoidType(MushroomParser.VoidTypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code voidType}
	 * labeled alternative in {@link MushroomParser#nonArrayType}.
	 * @param ctx the parse tree
	 */
	void exitVoidType(MushroomParser.VoidTypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code intType}
	 * labeled alternative in {@link MushroomParser#nonArrayType}.
	 * @param ctx the parse tree
	 */
	void enterIntType(MushroomParser.IntTypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code intType}
	 * labeled alternative in {@link MushroomParser#nonArrayType}.
	 * @param ctx the parse tree
	 */
	void exitIntType(MushroomParser.IntTypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code boolType}
	 * labeled alternative in {@link MushroomParser#nonArrayType}.
	 * @param ctx the parse tree
	 */
	void enterBoolType(MushroomParser.BoolTypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code boolType}
	 * labeled alternative in {@link MushroomParser#nonArrayType}.
	 * @param ctx the parse tree
	 */
	void exitBoolType(MushroomParser.BoolTypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code strType}
	 * labeled alternative in {@link MushroomParser#nonArrayType}.
	 * @param ctx the parse tree
	 */
	void enterStrType(MushroomParser.StrTypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code strType}
	 * labeled alternative in {@link MushroomParser#nonArrayType}.
	 * @param ctx the parse tree
	 */
	void exitStrType(MushroomParser.StrTypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code classType}
	 * labeled alternative in {@link MushroomParser#nonArrayType}.
	 * @param ctx the parse tree
	 */
	void enterClassType(MushroomParser.ClassTypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code classType}
	 * labeled alternative in {@link MushroomParser#nonArrayType}.
	 * @param ctx the parse tree
	 */
	void exitClassType(MushroomParser.ClassTypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code boolConstant}
	 * labeled alternative in {@link MushroomParser#constant}.
	 * @param ctx the parse tree
	 */
	void enterBoolConstant(MushroomParser.BoolConstantContext ctx);
	/**
	 * Exit a parse tree produced by the {@code boolConstant}
	 * labeled alternative in {@link MushroomParser#constant}.
	 * @param ctx the parse tree
	 */
	void exitBoolConstant(MushroomParser.BoolConstantContext ctx);
	/**
	 * Enter a parse tree produced by the {@code nullConstant}
	 * labeled alternative in {@link MushroomParser#constant}.
	 * @param ctx the parse tree
	 */
	void enterNullConstant(MushroomParser.NullConstantContext ctx);
	/**
	 * Exit a parse tree produced by the {@code nullConstant}
	 * labeled alternative in {@link MushroomParser#constant}.
	 * @param ctx the parse tree
	 */
	void exitNullConstant(MushroomParser.NullConstantContext ctx);
	/**
	 * Enter a parse tree produced by the {@code intConstant}
	 * labeled alternative in {@link MushroomParser#constant}.
	 * @param ctx the parse tree
	 */
	void enterIntConstant(MushroomParser.IntConstantContext ctx);
	/**
	 * Exit a parse tree produced by the {@code intConstant}
	 * labeled alternative in {@link MushroomParser#constant}.
	 * @param ctx the parse tree
	 */
	void exitIntConstant(MushroomParser.IntConstantContext ctx);
	/**
	 * Enter a parse tree produced by the {@code strConstant}
	 * labeled alternative in {@link MushroomParser#constant}.
	 * @param ctx the parse tree
	 */
	void enterStrConstant(MushroomParser.StrConstantContext ctx);
	/**
	 * Exit a parse tree produced by the {@code strConstant}
	 * labeled alternative in {@link MushroomParser#constant}.
	 * @param ctx the parse tree
	 */
	void exitStrConstant(MushroomParser.StrConstantContext ctx);
}