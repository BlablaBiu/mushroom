// Generated from /Users/blacko/Git/mushroom-compiler/Mushroom/src/compiler/Paser/Mushroom.g4 by ANTLR 4.6
package compiler.Paser;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link MushroomParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface MushroomVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link MushroomParser#program}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProgram(MushroomParser.ProgramContext ctx);
	/**
	 * Visit a parse tree produced by {@link MushroomParser#classDec}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClassDec(MushroomParser.ClassDecContext ctx);
	/**
	 * Visit a parse tree produced by {@link MushroomParser#funcDec}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFuncDec(MushroomParser.FuncDecContext ctx);
	/**
	 * Visit a parse tree produced by the {@code toBlockStat}
	 * labeled alternative in {@link MushroomParser#stat}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitToBlockStat(MushroomParser.ToBlockStatContext ctx);
	/**
	 * Visit a parse tree produced by the {@code exprStat}
	 * labeled alternative in {@link MushroomParser#stat}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExprStat(MushroomParser.ExprStatContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ifStat}
	 * labeled alternative in {@link MushroomParser#stat}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIfStat(MushroomParser.IfStatContext ctx);
	/**
	 * Visit a parse tree produced by the {@code whileStat}
	 * labeled alternative in {@link MushroomParser#stat}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhileStat(MushroomParser.WhileStatContext ctx);
	/**
	 * Visit a parse tree produced by the {@code forStat}
	 * labeled alternative in {@link MushroomParser#stat}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitForStat(MushroomParser.ForStatContext ctx);
	/**
	 * Visit a parse tree produced by the {@code continueStat}
	 * labeled alternative in {@link MushroomParser#stat}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitContinueStat(MushroomParser.ContinueStatContext ctx);
	/**
	 * Visit a parse tree produced by the {@code breakStat}
	 * labeled alternative in {@link MushroomParser#stat}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBreakStat(MushroomParser.BreakStatContext ctx);
	/**
	 * Visit a parse tree produced by the {@code returnStat}
	 * labeled alternative in {@link MushroomParser#stat}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReturnStat(MushroomParser.ReturnStatContext ctx);
	/**
	 * Visit a parse tree produced by {@link MushroomParser#blockStat}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlockStat(MushroomParser.BlockStatContext ctx);
	/**
	 * Visit a parse tree produced by {@link MushroomParser#toVarDec}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitToVarDec(MushroomParser.ToVarDecContext ctx);
	/**
	 * Visit a parse tree produced by {@link MushroomParser#varDec}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVarDec(MushroomParser.VarDecContext ctx);
	/**
	 * Visit a parse tree produced by the {@code assignmentExpr}
	 * labeled alternative in {@link MushroomParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssignmentExpr(MushroomParser.AssignmentExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code funcExpr}
	 * labeled alternative in {@link MushroomParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFuncExpr(MushroomParser.FuncExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code newNonArrayExpr}
	 * labeled alternative in {@link MushroomParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNewNonArrayExpr(MushroomParser.NewNonArrayExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code arrayExpr}
	 * labeled alternative in {@link MushroomParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArrayExpr(MushroomParser.ArrayExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code memberExpr}
	 * labeled alternative in {@link MushroomParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMemberExpr(MushroomParser.MemberExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code binaryExpr}
	 * labeled alternative in {@link MushroomParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBinaryExpr(MushroomParser.BinaryExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code memberFuncExpr}
	 * labeled alternative in {@link MushroomParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMemberFuncExpr(MushroomParser.MemberFuncExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code error}
	 * labeled alternative in {@link MushroomParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitError(MushroomParser.ErrorContext ctx);
	/**
	 * Visit a parse tree produced by the {@code subExpr}
	 * labeled alternative in {@link MushroomParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSubExpr(MushroomParser.SubExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code varExpr}
	 * labeled alternative in {@link MushroomParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVarExpr(MushroomParser.VarExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code unaryExpr}
	 * labeled alternative in {@link MushroomParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnaryExpr(MushroomParser.UnaryExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code postfixExpr}
	 * labeled alternative in {@link MushroomParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPostfixExpr(MushroomParser.PostfixExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code newArrayExpr}
	 * labeled alternative in {@link MushroomParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNewArrayExpr(MushroomParser.NewArrayExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code constantExpr}
	 * labeled alternative in {@link MushroomParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConstantExpr(MushroomParser.ConstantExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code toNonArrayType}
	 * labeled alternative in {@link MushroomParser#type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitToNonArrayType(MushroomParser.ToNonArrayTypeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code arrayType}
	 * labeled alternative in {@link MushroomParser#type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArrayType(MushroomParser.ArrayTypeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code voidType}
	 * labeled alternative in {@link MushroomParser#nonArrayType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVoidType(MushroomParser.VoidTypeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code intType}
	 * labeled alternative in {@link MushroomParser#nonArrayType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntType(MushroomParser.IntTypeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code boolType}
	 * labeled alternative in {@link MushroomParser#nonArrayType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoolType(MushroomParser.BoolTypeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code strType}
	 * labeled alternative in {@link MushroomParser#nonArrayType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStrType(MushroomParser.StrTypeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code classType}
	 * labeled alternative in {@link MushroomParser#nonArrayType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClassType(MushroomParser.ClassTypeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code boolConstant}
	 * labeled alternative in {@link MushroomParser#constant}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoolConstant(MushroomParser.BoolConstantContext ctx);
	/**
	 * Visit a parse tree produced by the {@code nullConstant}
	 * labeled alternative in {@link MushroomParser#constant}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNullConstant(MushroomParser.NullConstantContext ctx);
	/**
	 * Visit a parse tree produced by the {@code intConstant}
	 * labeled alternative in {@link MushroomParser#constant}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntConstant(MushroomParser.IntConstantContext ctx);
	/**
	 * Visit a parse tree produced by the {@code strConstant}
	 * labeled alternative in {@link MushroomParser#constant}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStrConstant(MushroomParser.StrConstantContext ctx);
}