grammar Mushroom;

program
    : (classDec | funcDec | toVarDec)+ EOF
    ;

classDec
    : 'class' ID  '{' (funcDec | toVarDec)* '}'
    ;

funcDec
    : type? ID '(' (varDec (',' varDec)*)? ')' blockStat
    ;

stat
    : blockStat                                                             #toBlockStat
    | expr? ';'                                                             #exprStat
    | 'if' '(' expr ')' stat ('else' stat)?                                 #ifStat
    | 'while' '(' expr ')' stat                                             #whileStat
    | 'for' '(' init = expr? ';' cond = expr? ';' step = expr? ')' stat     #forStat
    | 'continue' ';'                                                        #continueStat
    | 'break' ';'                                                           #breakStat
    | 'return' expr? ';'                                                    #returnStat
    ;

blockStat
    : '{' (toVarDec | stat)* '}'
    ;

toVarDec
    : varDec ';'
    ;

varDec
    : type ID ('=' expr)?
    ;

expr
    : constant                                                          #constantExpr
    | 'new' nonArrayType ('[' expr ']')+ ('[' ']') + ('[' expr ']')+    #error
    | 'new' nonArrayType  ('[' expr ']')+ ('[' ']')*                    #newArrayExpr
    | 'new' nonArrayType                                                #newNonArrayExpr
    | ID                                                                #varExpr
    | '(' expr ')'                                                      #subExpr
    | ID '(' (expr (',' expr)*)? ')'                                    #funcExpr
    | expr '[' expr ']'                                                 #arrayExpr
    | expr '.' ID '('(expr (',' expr)*)?')'                             #memberFuncExpr
    | expr '.' ID                                                       #memberExpr
    | expr op=('++' | '--')                                             #postfixExpr

    | <assoc=right> op=('+' | '-' | '++' | '--' | '!' | '~') expr       #unaryExpr

    | expr op=('*' | '/' | '%') expr                                    #binaryExpr
    | expr op=('+' | '-') expr                                          #binaryExpr
    | expr op=('<<' | '>>') expr                                        #binaryExpr
    | expr op=('>' | '>=' | '<' | '<=') expr                            #binaryExpr
    | expr op=('==' | '!=' ) expr                                       #binaryExpr
    | expr op='&' expr                                                  #binaryExpr
    | expr op='^' expr                                                  #binaryExpr
    | expr op='|' expr                                                  #binaryExpr
    | expr op='&&' expr                                                 #binaryExpr
    | expr op='||' expr                                                 #binaryExpr

    | <assoc=right> expr '=' expr                                       #assignmentExpr
    ;

type
    : nonArrayType           #toNonArrayType
    | nonArrayType ('['']')* #arrayType
    ;

nonArrayType
    : 'void'        #voidType
    | 'int'         #intType
    | 'bool'        #boolType
    | 'string'      #strType
    | ID            #classType
    ;


constant
    : ('true' | 'false')    #boolConstant
    | 'null'                #nullConstant
    | Int                   #intConstant
    | Str                   #strConstant
    ;

ID
    : [a-zA-Z_][a-zA-Z0-9_]*
    ;

Int
    : [0-9]+
    ;

Str
    : '\"' (Esc | .)*? '\"'
    ;

Commment
    : '//' .*? Eol -> skip
    ;

WS
    : [ \t\n\r]+ -> skip
    ;

fragment Esc : '\\"' | '\\n' | '\\\\';
fragment Eol : '\r\n' | '\r' | '\n' ;