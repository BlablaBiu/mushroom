package compiler;

import compiler.AST.Declare.VarDec;
import compiler.IR.Function;
import compiler.IR.IR;
import compiler.IR.IRInstrution.*;
import compiler.IR.Operand.ImmediateNumber;
import compiler.IR.Operand.Address.MemoryAddress;
import compiler.IR.Operand.Operand;
import compiler.IR.Operand.Register.Register;
import compiler.IR.Operand.Register.VirtualRegister;
import compiler.IR.Operand.Register.X86Reg;

/**
 * Created by blacko on 2017/5 /27.
 */
public class X86translator implements IrVisitor {
    String X86Code = "";
    Function curFunc;
    X86Reg rax = X86Reg.rax;
    X86Reg rcx = X86Reg.rcx;
    X86Reg rdx = X86Reg.rdx;
    X86Reg rbp = X86Reg.rbp;
    X86Reg rsp = X86Reg.rsp;
    public void visit(IrInstruction x){ x.visit(this);}
    String translate(IR x) {
        X86Code += "global main\n\n";
        X86Code += "section .data\n";
        for(VarDec item : x.globalVariables) {
            X86Code += item.ID + "__" + ":\n";
            X86Code += "\tdq\t0\n";
        }
        int cnt = 0;
        for(String item : x.stringConstants) {
            X86Code += "\tdq\t" + item.length() + "\n";
            X86Code += "string__" + Integer.toString(cnt++) + ":\n";
            X86Code += "\tdb\t";
            for(int i = 0; i < item.length(); ++i) {
                X86Code += Integer.toString((int)item.charAt(i)) + ", ";
            }
            X86Code += "0\n";

        }
        X86Code += "\nsection .text\n";
        for(Function item : x.functions) {

            int Max = 0;
            for(int i = 0; i < item.regList.size(); ++i) {
                int tmpMax = item.regList.get(i).index;
                //System.out.println(X86Reg.getByRank(item.regList.get(i).index));
                if(tmpMax > Max) Max = tmpMax;
            }
            // getMaxUse
            Max = (Max - 15);
            if(Max < 0) Max = 0;
            if(Max % 2 == 1) ++Max;
            item.maxSize = Max * 8;
            //System.out.println(Max);
            // getMaxReg
            item.MaxReg = 0;
            if(item.useRegTag != null) {
                for (int i = 0; i < 11; ++i)
                    if (item.useRegTag[i]) {
                        item.MaxReg += 1;
                    }
            } else {
                item.MaxReg = 0;
            }
            X86Code += item.name + ":\n";
            curFunc = item;
            visit(new EnterFunc());
            for(IrInstruction ins : item.insList) {
                visit(ins);
            }
            visit(new LeaveFunc());
        }
        addBuilt_in();
        return X86Code;
    }
    private Operand justMakeIt(Operand x,X86Reg tmpReg) {
        if(x instanceof VirtualRegister) {
            VirtualRegister tmp = (VirtualRegister) x;
//            return new MemoryAddress(rbp, null, 0, -(tmp.index + 1) * 8);
            if(tmp.index < 16) {
                return X86Reg.ToX86(tmp.index);
            }
            return new MemoryAddress(rbp, null, 0, -(tmp.index - 16 + 1) * 8);
        }
        if(x instanceof MemoryAddress) {
            MemoryAddress tmp = (MemoryAddress) x;
            Operand baseReg = justMakeIt(tmp.baseReg, null);
            Operand indexReg = justMakeIt(tmp.indexReg, null);
            if(indexReg instanceof MemoryAddress || baseReg instanceof MemoryAddress) {
                if(indexReg != null) {
                    X86Code += "\tmov\t\t" + tmpReg.toString() + ", " + indexReg.toString() + "\n";
                    X86Code += "\timul\t" + tmpReg.toString() + ", " + Integer.toString(tmp.scale) + "\n";
                    X86Code += "\tadd\t\t" + tmpReg.toString() + ", " + baseReg.toString() + "\n";
                } else {
                    X86Code  += "\tmov\t\t" + tmpReg.toString() + ", " + baseReg.toString() + "\n";
                }
                return new MemoryAddress(tmpReg, null, 0, tmp.disp);
            } else {
                return new MemoryAddress((Register) baseReg, (Register) indexReg, tmp.scale, tmp.disp);
            }
        }
        return x;
    }

    private void solveA(Operand dest, Operand lhs, Operand rhs, String op) {
        dest = justMakeIt(dest, rcx);
        lhs = justMakeIt(lhs, rdx);
        if(lhs == dest && dest instanceof Register) {
            rhs = justMakeIt(rhs, rdx);
            X86Code += "\t"+ op +"\t\t" + dest.toString() + ", " + rhs.toString() + "\n";
            return;
        }
        rhs = justMakeIt(rhs, rax);
        //--
//        if(rhs != rax) {
//            X86Code += "\tmov\t\trax, " + rhs.toString() + "\n";
//            rhs = rax;
//        }
        //---
        if(op.equals("add")) {
            if (dest instanceof Register && lhs instanceof Register && (rhs instanceof Register || rhs instanceof ImmediateNumber)){
                X86Code += "\tlea\t\t" + dest.toString() + ", [" + lhs.toString()+ " + " + rhs.toString() + "]\n";
                return;
            }
        }
        if(lhs != rdx) {
            X86Code += "\tmov\t\trdx, " + lhs.toString() + "\n";
            lhs = rdx;
        }
//        X86Code += "\t"+ op +"\t\tedx, eax\n";
        if(op.equals("imul")) {
            X86Code += "\t"+ op +"\trdx, " + rhs.toString() + "\n";
        } else {
            X86Code += "\t"+ op +"\trdx, " + rhs.toString() + "\n";
        }
        X86Code += "\tmov\t\t" + dest.toString() + ", rdx\n";
    }
    private void solveD(Operand dest, Operand lhs, Operand rhs, boolean isDiv) {
        dest = justMakeIt(dest, null);
        lhs = justMakeIt(lhs, rax);
        if(lhs != rax) {
            X86Code += "\tmov\t\trax, " + lhs.toString() + "\n";
            lhs = rax;
        }
        rhs = justMakeIt(rhs, rcx);
        if(rhs != rcx) {
            X86Code += "\tmov\t\trcx, " + rhs.toString() + "\n";
            rhs = rcx;
        }
        X86Code += "\tcqo\n";
        X86Code += "\tidiv\tecx\n";
        if(isDiv) {
            X86Code += "\tmov\t\t" +  dest.toString() +  ", rax\n";
        } else {
            X86Code += "\tmov\t\t" +  dest.toString() +  ", rdx\n";
        }
    }

    private void solveE(Operand dest, Operand lhs, Operand rhs, String op) {
        if(lhs instanceof ImmediateNumber && rhs instanceof  ImmediateNumber) {
            int tmp = 0;
            if(op.equals("shl")) {
                tmp = ((ImmediateNumber) lhs).value << ((ImmediateNumber) rhs).value;
            } else {
                tmp = ((ImmediateNumber) lhs).value >> ((ImmediateNumber) rhs).value;
            }
            X86Code += "\tmov\t\t" + dest.toString() + ", " + Integer.toString(tmp) + "\n";
            return;
        }
        rhs = justMakeIt(rhs, rcx);
        X86Code += "\tmov\t\trcx, " +  rhs.toString() +  "\n";
        rhs = rcx;
        lhs = justMakeIt(lhs, rax);
        X86Code += "\tmov\t\trax, " +  lhs.toString() +  "\n";
        lhs = rax;
        X86Code += "\t" + op + "\t\teax, cl\n";
        dest = justMakeIt(dest, rcx);
        X86Code += "\tmov\t\t" + dest.toString() + ", rax\n";
    }

    private boolean equal(Operand lhs, Operand rhs) {
        if(lhs == rhs) return  true;
        if(lhs instanceof VirtualRegister && rhs instanceof VirtualRegister) {
            if(((VirtualRegister) lhs).index == ((VirtualRegister) rhs).index) {
                return true;
            }
        }
        return false;
    }


    private void solveC(Operand dest, Operand lhs, Operand rhs, String op) {
        dest = justMakeIt(dest, rax);
        lhs  = justMakeIt(lhs, rcx); // rcx
        rhs  = justMakeIt(rhs, rdx); // rax
        if(!(lhs instanceof Register) && !(rhs instanceof Register)) {
            X86Code += "\tmov\t\trcx, " + lhs.toString() + "\n";
            lhs = rcx;
        }
        if(lhs instanceof ImmediateNumber) {
            X86Code += "\tmov\t\trcx, " + lhs.toString() + "\n";
            lhs = rcx;
        }
        X86Code += "\tcmp\t\t" + lhs.toString() + ", " + rhs.toString() + "\n";
        X86Code += "\t" + op + "\tcl\n";
        X86Code += "\tmovzx\trcx, cl\n";
        X86Code += "\tmov\t\t" + dest.toString() + ", rcx\n";
    }

    public void visit(BinaryOperation x) {
        switch ((x.op)) {
            case "==" : solveC(x.dest, x.lhs, x.rhs, "sete"); break;
            case "!=" : solveC(x.dest, x.lhs, x.rhs, "setne"); break;
            case ">"  : solveC(x.dest, x.lhs, x.rhs, "setg"); break;
            case ">=" : solveC(x.dest, x.lhs, x.rhs, "setge"); break;
            case "<"  : solveC(x.dest, x.lhs, x.rhs, "setl"); break;
            case "<=" : solveC(x.dest, x.lhs, x.rhs, "setle"); break;
            case "+" : solveA(x.dest, x.lhs, x.rhs, "add"); break;
            case "-" : solveA(x.dest, x.lhs, x.rhs, "sub"); break;
            case "&" : solveA(x.dest, x.lhs, x.rhs, "and"); break;
            case "|" : solveA(x.dest, x.lhs, x.rhs, "or"); break;
            case "^" : solveA(x.dest, x.lhs, x.rhs, "xor"); break;
            case "*" : solveA(x.dest, x.lhs, x.rhs, "imul"); break;
            case "/" : solveD(x.dest, x.lhs, x.rhs, true);break;
            case "%" : solveD(x.dest, x.lhs, x.rhs, false);break;
            case "<<" : solveE(x.dest, x.lhs, x.rhs, "shl"); break;
            case ">>" : solveE(x.dest, x.lhs, x.rhs, "sar"); break;
            case "&&" :
            case "||" :
            default:
                throw new RuntimeException("???");
        }
    }

    public void visit(CmpAndJump x){
        Operand lhs = justMakeIt(x.lhs, rax);
        Operand rhs = justMakeIt(x.rhs, rcx);
        if(!(lhs instanceof Register)) {
            X86Code += "\tmov\t\trax, " + lhs.toString() + "\n";
            lhs = X86Reg.rax;
        }
        X86Code += "\tcmp\t\t" + lhs.toString() + ", " + rhs.toString() + "\n";
        X86Code += "\t" + x.op + "\t\t" + x.target.name + "\n";
    }

    public void visit(Move x){
        if(equal(x.dest, x.source)) return;
        Operand dest = justMakeIt(x.dest, rcx);
        Operand source = justMakeIt(x.source, rax); // rax
        if(!(dest instanceof Register) && !(source instanceof Register)) {
            X86Code += "\tmov\t\trax, " + source.toString() + "\n";
            source = X86Reg.rax;
        }
        X86Code += "\tmov\t\t" + dest.toString() + ", " + source.toString() + "\n";
    }

    public void visit(UnaryOperation x){
        Operand tmpReg, tmp = justMakeIt(x.dest, rax);
        boolean useReg = false;
        if(!(tmp instanceof Register)) {
            X86Code += "\tmov\t\trax ," + tmp.toString() + "\n";
            tmpReg = rax;
            useReg = true;
        } else {
            tmpReg = tmp;
        }
        switch (x.op) {
            case "!" :
                X86Code += "\txor\t\t" + tmpReg.toString() + ", 1\n";
                break;
            case "+" :
                break;
            case "-" :
                X86Code += "\tneg\t\t" + tmpReg.toString() + "\n";
                break;
            case "~" :
                X86Code += "\tnot\t\t" + tmpReg.toString() + "\n";
                break;
            case "++" :
            case "--":
            default:
                throw new RuntimeException("???");
        }
        if(useReg) {
            X86Code += "\tmov\t\t" + tmp.toString() + ", rax\n";
        }
    }

    public void visit(Call x) {
//        for(IrInstruction item : x.callIr) {
//            if(item instanceof CallFunc) {
//                if (((CallFunc) item).funcName.equals("String_ord")){
//                    X86Code += "\tmov rax,0\n" +
//                            "\tmov al,byte[rdi+rsi]\n";
//                }
//            }
//            visit(item);
//        }
        int tmpStack = 0;
        if(curFunc.name.equals("main")) {
            if (x.size > 6) tmpStack = (x.size - 6) * 8;
        } else {
            if (x.size > 6) {
                tmpStack = (x.size - 6) * 8;
                if (x.size % 2 == 1 && curFunc.MaxReg % 2 == 0) {
                    tmpStack += 8;
                }
                if (x.size % 2 == 0 && curFunc.MaxReg % 2 == 1) {
                    tmpStack += 8;
                }
            } else {
                if (curFunc.MaxReg % 2 == 1) {
                    tmpStack += 8;
                }
            }
        }
        if(tmpStack != 0) X86Code += "\tsub\t\trsp, " + Integer.toString(tmpStack) + "\n";
        if(curFunc.optim) {
            for (int i = 0; i < 6; ++i) {
                if(!curFunc.useRegTag[X86Reg.getCaller(i).index]) continue;
                X86Code += "\tpush\t" + X86Reg.getCaller(i).toString() + "\n";
            }
        }
        for(IrInstruction item : x.callIr) {
            visit(item);
            if(item instanceof CallFunc && curFunc.optim) {
                for (int i = 5; i >= 0; --i) {
                    if(!curFunc.useRegTag[X86Reg.getCaller(i).index]) continue;
                    X86Code += "\tpop\t\t" + X86Reg.getCaller(i).toString() + "\n";
                }
            }
        }
        if(tmpStack != 0) X86Code += "\tadd\t\trsp, " + Integer.toString(tmpStack) + "\n";
    }

    public void visit(CallFunc x) { X86Code += "\tcall\t" + x.funcName + "\n"; }
    public void visit(Jump x){
        X86Code += "\tjmp\t\t" + x.label.name + "\n";
    }

    public void visit(Label x){
        X86Code += x.name + ":\n";
    }

    int SaveCnt = 0;
    public void visit(EnterFunc x){
        X86Code += "\tpush\trbp\n";
        X86Code += "\tmov\t\trbp, rsp\n";
        SaveCnt = 0;
        if(curFunc.optim && !curFunc.name.equals("main")) {
            for (int i = 0; i < 5; ++i) {
                if(!curFunc.useRegTag[X86Reg.getCallee(i).index]) continue;
                X86Code += "\tpush\t" + X86Reg.getCallee(i).toString() + "\n";
                ++SaveCnt;
            }
        }
        if(SaveCnt != 0) {
            X86Code += "\tsub\t\trbp, " + Integer.toString((SaveCnt * 8)) + "\n";
        }
        if(curFunc.maxSize != 0) {
            X86Code += "\tsub\t\trsp, " + Integer.toString((curFunc.maxSize)) + "\n";
        }
    }

    public void visit(LeaveFunc x){
        if(curFunc.maxSize != 0) {
            X86Code += "\tadd\t\trsp, " + Integer.toString((curFunc.maxSize)) + "\n";
        }
        if(SaveCnt != 0) {
            X86Code += "\tadd\t\trbp, " + Integer.toString((SaveCnt * 8)) + "\n";
        }
        if(curFunc.optim && !curFunc.name.equals("main")) {
            for (int i = 4; i >= 0; --i) {
                if(!curFunc.useRegTag[X86Reg.getCallee(i).index]) continue;
                X86Code += "\tpop\t\t" + X86Reg.getCallee(i).toString() + "\n";
            }
        }
        X86Code += "\tmov\t\trsp, rbp\n";
        X86Code += "\tpop\t\trbp\n";
        X86Code += "\tret\t\n";
    }

    private void addBuilt_in() {
        X86Code += "\n\n;thanks for XZYY's inner-built function assembly code\n" +
                "\n" +
                "section .data\n" +
                "intbuffer:\n" +
                "\tdq 0\n" +
                "format1:\n" +
                "\tdb\"%lld\",0\n" +
                "format2:\n" +
                "\tdb\"%lld \",10,0\n" +
                "format3:\n" +
                "\tdb\"%s\",0\n" +
                "format4:\n" +
                "\tdb\"%s\",10,0\n" +
                "\n" +
                "section .bss\n" +
                "stringbuffer:\n" +
                "\tresb 256\n" +
                "\n" +
                "extern scanf\n" +
                "extern printf\n" +
                "extern puts\n" +
                "extern strlen\n" +
                "extern memcpy\n" +
                "extern sscanf\n" +
                "extern sprintf\n" +
                "extern malloc\n" +
                "extern strcmp\n" +
                "\n" +
                "section .text\n" +
                "getInt:\n" +
                "\n" +
                "\tpush rbp\n" +
                "\tmov rbp,rsp\n" +
                "\tmov rax,0\n" +
                "\tmov rdi,format1\n" +
                "\tmov rsi,intbuffer\n" +
                "\tcall scanf\n" +
                "\tmov rax,[intbuffer]\n" +
                "\tmov rsp,rbp\n" +
                "\tpop rbp\n" +
                "\tret\n" +
                "\n" +
                "printInt:\n" +
                "\tpush rbp\n" +
                "\tmov rbp,rsp\n" +
                "\tmov rsi,rdi\n" +
                "\tmov rax,0\n" +
                "\tmov rdi,format2\n" +
                "\tcall printf\n" +
                "\tmov rsp,rbp\n" +
                "\tpop rbp\n" +
                "\tret\n" +
                "\n" +
                "Array_size:\n" +
                "\tmov rax,[rdi-8]\n" +
                "\tret\n" +
                "\n" +
                "print:\n" +
                "\tpush rbp\n" +
                "\tmov rbp,rsp\n" +
                "\tmov rax,0\n" +
                "\tmov rsi,rdi\n" +
                "\tmov rdi,format3\n" +
                "\tcall printf\n" +
                "\tmov rsp,rbp\n" +
                "\tpop rbp\n" +
                "\tret\n" +
                "\n" +
                "println:\n" +
                "\n" +
                "\tcall puts\n" +
                "\tret\n" +
                "\n" +
                "\n" +
                "transtring:\n" +
                "\n" +
                "\tpush rbp\n" +
                "\tmov rbp,rsp\n" +
                "\tcall strlen\n" +
                "\tpush rdi\n" +
                "\tmov rdi,rax\n" +
                "\tpush rdi\n" +
                "\tadd rdi,9\n" +
                "\tcall malloc\n" +
                "\tpop rdi\n" +
                "\tmov [rax],rdi\n" +
                "\tadd rax,8\n" +
                "\tmov rdx,rdi\n" +
                "\tadd rdx,1\n" +
                "\tmov rdi,rax\n" +
                "\tpop rsi\n" +
                "\tsub rsp,8\n" +
                "\tpush rax\n" +
                "\tcall memcpy\n" +
                "\tpop rax\n" +
                "\tmov rsp,rbp\n" +
                "\tpop rbp\n" +
                "\tret\n" +
                "\n" +
                "getString:\n" +
                "\n" +
                "\tpush rbp\n" +
                "\tmov rbp,rsp\n" +
                "\tmov rax,0\n" +
                "\tmov rdi,format3\n" +
                "\tmov rsi,stringbuffer\n" +
                "\tcall scanf\n" +
                "\tmov rdi,stringbuffer\n" +
                "\tcall transtring\n" +
                "\tmov rsp,rbp\n" +
                "\tpop rbp\n" +
                "\tret\n" +
                "\n" +
                "toString:\n" +
                "\n" +
                "\tpush rbp\n" +
                "\tmov rbp,rsp\n" +
                "\tmov rdx,rdi\n" +
                "\tmov rax,0\n" +
                "\tmov rdi,stringbuffer\n" +
                "\tmov rsi,format1\n" +
                "\tcall sprintf\n" +
                "\tmov rdi,stringbuffer\n" +
                "\tcall transtring\n" +
                "\tmov rsp,rbp\n" +
                "\tpop rbp\n" +
                "\tret\n" +
                "\n" +
                "String_length:\n" +
                "\n" +
                "\tmov rax,[rdi-8]\n" +
                "\tret\n" +
                "\n" +
                "String_substring:\n" +
                "\n" +
                "\tpush rbp\n" +
                "\tmov rbp,rsp\n" +
                "\tpush rdi\n" +
                "\tpush rsi\n" +
                "\tmov rdi,rdx\n" +
                "\tsub rdi,rsi\n" +
                "\tadd rdi,1\n" +
                "\tpush rdi\n" +
                "\tadd rdi,9\n" +
                "\tcall malloc\n" +
                "\tpop rdx\n" +
                "\tmov [rax],rdx\n" +
                "\tadd rax,8\n" +
                "\tpop rsi\n" +
                "\tpop rdi\n" +
                "\tadd rsi,rdi\n" +
                "\tmov rdi,rax\n" +
                "\tpush rdx\n" +
                "\tpush rax\n" +
                "\tcall memcpy\n" +
                "\tpop rax\n" +
                "\tpop rdx\n" +
                "\tmov qword[rax+rdx],0\n" +
                "\tmov rsp,rbp\n" +
                "\tpop rbp\n" +
                "\tret\n" +
                "\n" +
                "String_parseInt:\n" +
                "\n" +
                "\tmov rsi,format1\n" +
                "\tmov rdx,intbuffer\n" +
                "\tmov rax,0\n" +
                "\tcall sscanf\n" +
                "\tmov rax,[intbuffer]\n" +
                "\tret\n" +
                "\n" +
                "String_ord:\n" +
                "\n" +
                "\tmov rax,0\n" +
                "\tmov al,byte[rdi+rsi]\n" +
                "\tret\n" +
                "\n" +
                "String_add:\n" +
                "\n" +
                "\tpush rbp\n" +
                "\tmov rbp,rsp\n" +
                "\tpush rsi\n" +
                "\tmov rsi,rdi\n" +
                "\tmov rdi,stringbuffer\n" +
                "\tmov rdx,[rsi-8]\n" +
                "\tpush rdx\n" +
                "\tcall memcpy\n" +
                "\tpop rdi\n" +
                "\tpop rsi\n" +
                "\tadd rdi,stringbuffer\n" +
                "\tmov rdx,[rsi-8]\n" +
                "\tadd rdx,1\n" +
                "\tcall memcpy\n" +
                "\tmov rdi,stringbuffer\n" +
                "\tcall transtring\n" +
                "\tmov rsp,rbp\n" +
                "\tpop rbp\n" +
                "\tret\n" +
                "\n" +
                "String_lt:\n" +
                "\n" +
                "\tpush rbp\n" +
                "\tmov rbp,rsp\n" +
                "\tcall strcmp\n" +
                "\tmov rdi,0\n" +
                "\tcmp rax,0\n" +
                "\tsetl dil\n" +
                "\tmov rax,rdi\n" +
                "\tmov rsp,rbp\n" +
                "\tpop rbp\n" +
                "\tret\n" +
                "\n" +
                "String_le:\n" +
                "\n" +
                "\tpush rbp\n" +
                "\tmov rbp,rsp\n" +
                "\tcall strcmp\n" +
                "\tmov rdi,0\n" +
                "\tcmp rax,0\n" +
                "\tsetle dil\n" +
                "\tmov rax,rdi\n" +
                "\tmov rsp,rbp\n" +
                "\tpop rbp\n" +
                "\tret\n" +
                "\n" +
                "String_gt:\n" +
                "\n" +
                "\tpush rbp\n" +
                "\tmov rbp,rsp\n" +
                "\tcall strcmp\n" +
                "\tmov rdi,0\n" +
                "\tcmp rax,0\n" +
                "\tsetg dil\n" +
                "\tmov rax,rdi\n" +
                "\tmov rsp,rbp\n" +
                "\tpop rbp\n" +
                "\tret\n" +
                "\n" +
                "String_ge:\n" +
                "\n" +
                "\tpush rbp\n" +
                "\tmov rbp,rsp\n" +
                "\tcall strcmp\n" +
                "\tmov rdi,0\n" +
                "\tcmp rax,0\n" +
                "\tsetge dil\n" +
                "\tmov rax,rdi\n" +
                "\tmov rsp,rbp\n" +
                "\tpop rbp\n" +
                "\tret\n" +
                "\n" +
                "String_eq:\n" +
                "\n" +
                "\tpush rbp\n" +
                "\tmov rbp,rsp\n" +
                "\tcall strcmp\n" +
                "\tmov rdi,0\n" +
                "\tcmp rax,0\n" +
                "\tsete dil\n" +
                "\tmov rax,rdi\n" +
                "\tmov rsp,rbp\n" +
                "\tpop rbp\n" +
                "\tret\n" +
                "\n" +
                "String_ne:\n" +
                "\n" +
                "\tpush rbp\n" +
                "\tmov rbp,rsp\n" +
                "\tcall strcmp\n" +
                "\tmov rdi,0\n" +
                "\tcmp rax,0\n" +
                "\tsetne dil\n" +
                "\tmov rax,rdi\n" +
                "\tmov rsp,rbp\n" +
                "\tpop rbp\n" +
                "\tret";
    }
}
