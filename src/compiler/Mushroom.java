package compiler;

import compiler.IR.IR;
import compiler.Paser.MushroomLexer;
import compiler.Paser.MushroomParser;
import compiler.AST.*;
import org.antlr.v4.runtime.*;


import java.io.*;

public class Mushroom {
    static boolean SUBMIT = true;
    int I_do;

    private static void input(InputStream is) throws Exception {
        ANTLRInputStream input = new ANTLRInputStream(is);
        MushroomLexer lexer = new MushroomLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        MushroomParser parser = new MushroomParser(tokens);

        parser.removeErrorListeners();
        parser.addErrorListener(new SyntaxErrorListener());

        ASTBuilder ASTBuilder = new ASTBuilder();
        Program program = (Program) ASTBuilder.visit(parser.program());
        if (SUBMIT == false) {
            String ASTstr = program.toString("");
            File f = new File("/Users/blacko/Git/Mushroom/src/testcase/AST.out");
            PrintStream fout = new PrintStream(new FileOutputStream(f));
            fout.print(ASTstr);
        }

        IRBuilder IRuilder = new IRBuilder();
        IR myIR = IRuilder.getIR(program);

        RegisterAllocator biu = new RegisterAllocator(myIR);
        myIR = biu.niceShoot();

        if (SUBMIT == false) {
            String IRstr = myIR.toSring();
            File f = new File("/Users/blacko/Git/Mushroom/src/testcase/IR.out");
            PrintStream fout = new PrintStream(new FileOutputStream(f));
            fout.print(IRstr);
        }


        X86translator X86 = new X86translator();
        String X86Code = X86.translate(myIR);
        if (SUBMIT == false) {
            File f = new File("/Users/blacko/Git/Mushroom/src/testcase/X86.out");
            PrintStream fout = new PrintStream(new FileOutputStream(f));
            fout.print(X86Code);
        } else {
            System.out.print(X86Code);
        }
    }

    public static void main(String[] args) throws Exception {

        InputStream is;
        if(SUBMIT)  {
            is = System.in;
        } else {
            is = new FileInputStream("/Users/blacko/Git/Mushroom/src/testcase/testCase.txt");
        }
        input(is);
    }
}
