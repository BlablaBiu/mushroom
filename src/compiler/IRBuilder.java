package compiler;

import compiler.AST.AstVisitor;
import compiler.AST.Basic.*;
import compiler.AST.Basic.BasicType;
import compiler.AST.Declare.*;
import compiler.AST.Expression.*;
import compiler.AST.Program;
import compiler.AST.Statment.*;
import compiler.AST.Type.*;
import compiler.AST.Type.ArrayType;
import compiler.IR.*;
import compiler.IR.IRInstrution.*;
import compiler.IR.Operand.*;
import compiler.IR.Operand.Address.MemoryAddress;
import compiler.IR.Operand.Register.X86Reg;
import compiler.IR.Operand.Register.Register;
import compiler.IR.Operand.Register.VirtualRegister;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by blacko on 2017/5/17.
 */
public class IRBuilder implements AstVisitor {
    private IR root = new IR();
    boolean inLine = false;
    Function curFunction = null;
    private List<VarDec> initialExprs = new ArrayList<>();
    private Label continueLabel, breakLabel, returnLabel;
    private HashMap<String, Operand> trickMap;
    public IR getIR(Program x) { visit(x); return root;}

    public void visit(BasicNode x) { x.visit(this);}
    public void visit(BasicDec x) { x.visit(this);}
    public void visit(BasicStat x) { trickMap = null; x.visit(this);}

    public void visit(BasicExpr x) {
        boolean flag = true;
        if(trickMap != null && trickMap.containsKey(x.toString(""))) {
            //System.out.println(x.toString(""));
            x.operand = trickMap.get(x.toString(""));
            return;
        }
        x.visit(this);
        if(trickMap != null) {
            if(x instanceof NewArrayExpr) return;
            if(x instanceof NewExpr) return;
            if(x instanceof FuncExpr) return;
            if(x instanceof VarExpr) return;
            if(x instanceof MemberExpr) return;
            if(x instanceof ArrayExpr) return;
            trickMap.put(x.toString(""), x.operand);
        }
    }
    public void visit(BasicType x) { x.visit(this);}
    private VirtualRegister toReg(Operand x) {
        if(x instanceof Register) {
            return (VirtualRegister)x;
        } else {
            VirtualRegister tmp = curFunction.getReg();
            curFunction.addIns(new Move(tmp, x));
            return tmp;
        }
    }
    private int getSizeInClass(BasicType x) {
        return 8;
    }

    public void visit(Program x) {
        for(BasicDec item : x.decs) {
            if(item instanceof ClassDec) {
                int offset = 0;
                for (BasicDec i_item : ((ClassDec) item).decs) {
                    ((VarDec) i_item).isClassMember = true;
                    ((VarDec) i_item).offset = offset;
                    offset += getSizeInClass(((VarDec) i_item).type);
                }
                ((ClassDec) item).size = offset;
            } else if(item instanceof VarDec) {
                ((VarDec) item).isGlobal = true;
                ((VarDec) item).position = root.add(((VarDec) item));
                if(((VarDec) item).expr != null) {
                    initialExprs.add(((VarDec) item));
                }
            }
        }
        for(BasicDec item : x.decs) if(item instanceof FuncDec) visit(item);
    }

    public void visit(ClassDec x) {}

    public void visit(VarDec x) {
        if(trickMap == null) trickMap = new HashMap<>();
        //trickMap = null;
        if (x.expr != null) visit(x.expr);
        if (!x.isGlobal) x.position = curFunction.getReg();
        if (x.expr != null) {
            curFunction.addIns(new Move(x.position, x.expr.operand));
        } else {
            if(x.type instanceof ArrayType || x.type instanceof ClassType) {
                curFunction.addIns(new Move(x.position, new ImmediateNumber(0)));
            }
        }
    }

    public void visit(FuncDec x) {
        x.label = new Label(x.ID);
        curFunction = new Function(x.ID);
        returnLabel = new Label();
        root.add(curFunction);
        for(int i = 0; i < x.parameters.size(); ++i) {
            Operand source;
            if(i < 6) {
                VarDec tmp = x.parameters.get(i);
                visit(tmp);
                source = X86Reg.getParameterReg(i);
                curFunction.addIns(new Move(tmp.position, source));
            } else {
                x.parameters.get(i).position = new MemoryAddress(X86Reg.rbp, null, 0, 16 + (i - 6) * 8);
            }
        }
        if(x.ID.equals("main")) for(VarDec item : initialExprs) visit(item);
        visit(x.stat);
        curFunction.addIns(returnLabel);
        trickMap = null;
    }

    public void visit(AssignmentExpr x) {
        trickMap = null;
        visit(x.lExpr);
        trickMap = new HashMap<>();
        visit(x.rExpr);
        trickMap = null;
        curFunction.addIns(new Move(x.lExpr.operand, x.rExpr.operand));
        x.operand = null;
    }

    public void visit(ArrayExpr x) {
        visit(x.outExpr);
        visit(x.inExpr);
        Register indexReg = toReg(x.inExpr.operand);
        Register baseReg = toReg(x.outExpr.operand);
        x.operand = new MemoryAddress(baseReg, indexReg, 8, 0);
    }

    private Operand buildNewClass(BasicType x) {
        VirtualRegister dest = curFunction.getReg();
        curFunction.addIns(new Call("malloc", new ArrayList<Operand>(){{add(new ImmediateNumber(getSizeInNew(x)));}}, dest));
        if(x instanceof ClassType) {
            if(((ClassType) x).classDec.startFunc != null) {
                String funcName = ((ClassType) x).classDec.startFunc.ID;
                curFunction.addIns(new Call(funcName, new ArrayList<Operand>() {{
                    add(dest);
                }}, new MemoryAddress(dest, null, 0, 0)));
            }
        }
        return dest;
    }

    private Operand buildNewArray(NewArrayExpr x, int y) {
        visit(x.expDim.get(y));
        VirtualRegister size = curFunction.getReg();
        VirtualRegister dest = curFunction.getReg();
        curFunction.addIns(new Move(size, x.expDim.get(y).operand));
        curFunction.addIns(new BinaryOperation(size, "+", size, new ImmediateNumber(1)));
        curFunction.addIns(new BinaryOperation(size, "*", size, new ImmediateNumber(8)));
        curFunction.addIns(new Call("malloc", new ArrayList<Operand>(){{add(size);}}, dest));
        curFunction.addIns(new Move(new MemoryAddress(dest, null, 0, 0), x.expDim.get(y).operand));
        curFunction.addIns(new BinaryOperation(dest, "+", dest, new ImmediateNumber(8)));
        int flag = 0;
        if(x.emptyDim == 0 && x.bodyType instanceof ClassType && y + 1 == x.expDim.size()) {
            flag = 1; // new class;
        }
        if(y + 1 < x.expDim.size()) {
            flag = 2; // new array
        }
        if(flag != 0) {
            curFunction.addIns(new Move(size, x.expDim.get(y).operand));
            Label loop = new Label();
            curFunction.addIns(loop);
            curFunction.addIns(new BinaryOperation(size, "-", size, new ImmediateNumber(1)));
            if(flag == 1) {
                curFunction.addIns(new Move(new MemoryAddress(dest, size, 8, 0) ,buildNewClass(x.bodyType)));
            } else {
                curFunction.addIns(new Move(new MemoryAddress(dest, size, 8, 0) ,buildNewArray(x, y + 1)));
            }
            curFunction.addIns(new CmpAndJump(size, new ImmediateNumber(0), "jne", loop));
        }
        return dest;
    }

    private int getSizeInNew(BasicType x) {
        if(x instanceof ClassType) return ((ClassType) x).classDec.size;
        throw new RuntimeException("[You can't new this type]");
    }

    public void visit(NewArrayExpr x) {
        x.operand = buildNewArray(x, 0);
    }

    public void visit(NewExpr x) {
        x.operand = buildNewClass(x.type);
    }

    public void visit(BinaryExpr x) {
        x.operand = curFunction.getReg();
        visit(x.lExpr);
        if(x.op.equals("||")) {
            Label SuccLabel = new Label();
            Label OutLabel = new Label();
            curFunction.addIns(new CmpAndJump(x.lExpr.operand, new ImmediateNumber(1), "je", SuccLabel));
            visit(x.rExpr);
            if(trickMap != null) trickMap.remove(x.rExpr.toString(""));
            curFunction.addIns(new Move(x.operand, x.rExpr.operand));
            curFunction.addIns(new Jump(OutLabel));
            curFunction.addIns(SuccLabel);
            curFunction.addIns(new Move(x.operand, new ImmediateNumber(1)));
            curFunction.addIns(OutLabel);
            return;
        }
        if(x.op.equals("&&")){
            Label FailLabel = new Label();
            Label OutLabel = new Label();
            curFunction.addIns(new CmpAndJump(x.lExpr.operand, new ImmediateNumber(1), "je", FailLabel));
            curFunction.addIns(new Move(x.operand, new ImmediateNumber(0)));
            curFunction.addIns(new Jump(OutLabel));
            curFunction.addIns(FailLabel);
            visit(x.rExpr);
            if(trickMap != null) trickMap.remove(x.rExpr.toString(""));
            curFunction.addIns(new Move(x.operand, x.rExpr.operand));
            curFunction.addIns(OutLabel);
            return;
        }
        visit(x.rExpr);
        curFunction.addIns(new BinaryOperation(x.operand, x.op, x.lExpr.operand, x.rExpr.operand));
    }

    public void visit(ConstantExpr x) {
        if(x.type instanceof NullType) {
            x.operand = new ImmediateNumber(0);
        } else if(x.type instanceof StringType) {
            x.operand = root.add(x.valStr);
        } else if(x.type instanceof BoolType) {
            x.operand = new ImmediateNumber(x.valBool ? 1 : 0);
        } else if(x.type instanceof IntType) {
            x.operand = new ImmediateNumber(x.valInt);
        } else {
            throw new RuntimeException("[What type is it ???]");
        }
    }
    public void visit(FuncExpr x) {
        if(x.funcDec.ID.equals("String_length") || x.funcDec.ID.equals("Array_size")) {
            visit(x.parameters.get(0));
            Register tmp = toReg(x.parameters.get(0).operand);
            x.operand = new MemoryAddress(tmp, null, 0, -8);
            return ;
        }
        if(x.funcDec.ID.equals("String_ord")) {

        }
        List<Operand> parameters = new ArrayList<>();
        for(BasicExpr item : x.parameters) {
            visit(item);
            parameters.add(item.operand);
        }
        if(curFunction.numOfVirtualReg < 200 && x.funcDec.inline && x.funcDec.stat != null && x.funcDec.stat.VarDecAndStat.size() == 1) {
            if(x.funcDec.stat.VarDecAndStat.get(0) instanceof ReturnStat) {
                trickMap = null;
                x.funcDec.inline = false;
                BasicExpr returnExpr = ((ReturnStat) x.funcDec.stat.VarDecAndStat.get(0)).expr;
                HashMap<VarDec, Operand> newMap = new HashMap<>();
                for(int i = 0; i < x.funcDec.parameters.size(); ++i) {
                    VarDec A = x.funcDec.parameters.get(i);
                    BasicExpr B = x.parameters.get(i);
                    newMap.put(A, B.operand);
                }
                BasicExpr tmp = returnExpr.Inline(newMap);
                visit(tmp);
                x.operand = tmp.operand;
                x.funcDec.inline = true;
                return;
            }
        }
        x.operand = curFunction.getReg();
        Call call = new Call(x.funcDec.ID, parameters, x.operand);
        curFunction.addIns(call);
        trickMap = null;
    }

    public void visit(MemberExpr x) {
        visit(x.expr);
        Register baseReg = toReg(x.expr.operand);
        x.operand = new MemoryAddress(baseReg, null, 0, x.varDec.offset);
    }


    public void visit(PostfixExpr x) {
        visit(x.expr);
        trickMap = null;
        x.operand = curFunction.getReg();
        curFunction.addIns(new Move(x.operand, x.expr.operand));
        switch (x.op) {
            case "--" :
                curFunction.addIns(new BinaryOperation(x.expr.operand, "-", x.expr.operand, new ImmediateNumber(1)));
                break;
            case "++" :
                curFunction.addIns(new BinaryOperation(x.expr.operand, "+", x.expr.operand, new ImmediateNumber(1)));
                break;
            default:
                throw new RuntimeException("???");
        }
    }

    public void visit(UnaryExpr x) {
        visit(x.expr);
        switch (x.op) {
            case "--" :
                x.operand = x.expr.operand;
                curFunction.addIns(new BinaryOperation(x.operand, "-", x.operand, new ImmediateNumber(1)));
                break;
            case "++" :
                x.operand = x.expr.operand;
                curFunction.addIns(new BinaryOperation(x.operand, "+", x.operand, new ImmediateNumber(1)));
                break;
            default:
                x.operand = curFunction.getReg();
                curFunction.addIns(new Move(x.operand, x.expr.operand));
                curFunction.addIns(new UnaryOperation(x.operand, x.op));
                break;
        }
    }

    public void visit(VarExpr x) {
        x.operand = x.varDec.position;
    }

    public void visit(BlockStat x) {
        x.VarDecAndStat.forEach(item -> visit(item));
    }
    public void visit(ExprStat x) { if(x.expr != null) visit(x.expr); }

    public void visit(BreakStat x) {
        curFunction.addIns(new Jump(breakLabel));
    }
    public void visit(ContinueStat x) {
        curFunction.addIns(new Jump(continueLabel));
    }

    public void visit(ForStat x) {
        Label temContinueLabel = continueLabel;
        Label temBreakLabel = breakLabel;

        Label startLabel = new Label();
        Label trueLabel = new Label();
        continueLabel = new Label();
        breakLabel = new Label();
        if(x.init != null) visit(x.init);
        curFunction.addIns(startLabel);
        if (x.cond != null) {
            visit(x.cond);
            curFunction.addIns(new CmpAndJump(x.cond.operand, new ImmediateNumber(1), "je", trueLabel));
            curFunction.addIns(new Jump(breakLabel));
        } else {
            curFunction.addIns(new Jump(trueLabel));
        }
        curFunction.addIns(trueLabel);
        visit(x.stat);
        curFunction.addIns(continueLabel);
        if(x.step != null) visit(x.step);
        curFunction.addIns(new Jump(startLabel));
        curFunction.addIns(breakLabel);

        continueLabel = temContinueLabel;
        breakLabel = temBreakLabel;
    }

    public void visit(IfStat x) {
        visit(x.expr);
        Label trueLabel = new Label();
        Label falseLabel = new Label();
        Label endLabel = new Label();
        curFunction.addIns(new CmpAndJump(x.expr.operand, new ImmediateNumber(1), "je", trueLabel));
        curFunction.addIns(new Jump(falseLabel));
        curFunction.addIns(trueLabel);
        visit(x.trueStat);
        curFunction.addIns(new Jump(endLabel));
        curFunction.addIns(falseLabel);
        if(x.falseStat != null) {
            visit(x.falseStat);
        }
        curFunction.addIns(endLabel);
    }

    public void visit(ReturnStat x) {
        if(x.expr != null) {
            visit(x.expr);
            curFunction.addIns(new Move(X86Reg.rax, x.expr.operand));
        }
        curFunction.addIns(new Jump(returnLabel));
    }


    public void visit(ArrayType x) {}
    public void visit(BoolType x) {}
    public void visit(ClassType x) {}
    public void visit(IntType x) {}
    public void visit(StringType x) {}
    public void visit(VoidType x) {}
}
