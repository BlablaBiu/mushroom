package compiler;

import compiler.IR.IRInstrution.*;
import compiler.IR.Operand.Address.MemoryAddress;
import compiler.IR.Operand.Operand;
import compiler.IR.Operand.Register.Register;
import compiler.IR.Operand.Register.X86Reg;

import java.util.Set;

/**
 * Created by blacko on 2017/5/30.
 */
public class InAndOut implements IrVisitor {
    boolean start = false;
    public void visit(IrInstruction x) { x.visit(this);}

    void putDest(Set<Register> in, Operand x) {
        if(x instanceof MemoryAddress) {
            if(((MemoryAddress) x).indexReg != null) in.add(((MemoryAddress) x).indexReg);
            if(((MemoryAddress) x).baseReg != null) in.add(((MemoryAddress) x).baseReg);
        }
    }
    void putSource(Set<Register> in, Operand x) {
        if(x instanceof Register) {
            in.add((Register) x);
        } else {
            putDest(in, x);
        }
    }
    void setDef(IrInstruction ir, Operand x) {
        if(x instanceof Register) ir.def = (Register) x;
    }
    public void visit(BinaryOperation x) {
        if(start) {
            putSource(x.in, x.lhs);
            putSource(x.in, x.rhs);
            putDest(x.in, x.dest);
            setDef(x, x.dest);
            return;
        }
    }

    public void visit(Call x) {
        if(start) {
            for(IrInstruction item : x.callIr) visit(item);
            return;
        }
    }

    public void visit(CmpAndJump x) {
        if(start) {
            putSource(x.in, x.lhs);
            putSource(x.in, x.rhs);
            return;
        }
    }

    public void visit(Move x) {
        if(start) {
            putSource(x.in, x.source);
            putDest(x.in, x.dest);
            setDef(x, x.dest);
            return;
        }
    }

    public void visit(UnaryOperation x) {
        if(start) {
            putSource(x.in, x.dest);
            setDef(x, x.dest);
            return;
        }
    }

    public void visit(CallFunc x) {
        if(start) {
            int tmp = x.size;
            if (tmp > 6) tmp = 6;
            putDest(x.in, X86Reg.rax);
            setDef(x, X86Reg.rax);
            for (int i = 0; i < tmp; ++i) putSource(x.in, X86Reg.getParameterReg(i));
            return;
        }
    }

    public void visit(Jump x) {
        if(start) { return; }
    }

    public void visit(Label x) {
        if(start) { return; }
    }

    public void visit(LeaveFunc x) {
        throw new RuntimeException("???");
    }

    public void visit(EnterFunc x) {
        throw new RuntimeException("???");
    }

}
