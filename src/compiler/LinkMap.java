package compiler;

import compiler.AST.Basic.BasicNode;
import compiler.AST.Basic.BasicType;
import compiler.AST.Declare.ClassDec;
import compiler.AST.Declare.FuncDec;
import compiler.AST.Declare.VarDec;
import compiler.AST.Type.ClassType;

import java.util.HashMap;

public class LinkMap {
    public LinkMap upperMap;
    private HashMap<String, BasicNode> hashMap = new HashMap<>();

    private LinkMap() { upperMap = null; }

    public LinkMap(LinkMap upperMap_) { upperMap = upperMap_; }

    public static LinkMap getGlobalMap(){
        LinkMap map = new LinkMap();
        map.addLink("print", Built_in.print_());
        map.addLink("printInt", Built_in.printInt_());
        map.addLink("println", Built_in.println_());
        map.addLink("getString", Built_in.getString_());
        map.addLink("getInt", Built_in.getInt_());
        map.addLink("toString", Built_in.toString_());
        map.addLink("Array_size", Built_in.arraySize());
        map.addLink("String_length", Built_in.stringLength());
        map.addLink("String_ord", Built_in.stringord());
        map.addLink("String_parseInt", Built_in.stringParseInt());
        map.addLink("String_substring", Built_in.stringSubString());
        map.addLink("String_add", Built_in.stringAdd());
        map.addLink("String_eq", Built_in.stringEQ());
        map.addLink("String_ne", Built_in.stringNE());
        map.addLink("String_lt", Built_in.stringLT());
        map.addLink("String_gt", Built_in.stringGT());
        map.addLink("String_le", Built_in.stringLE());
        map.addLink("String_ge", Built_in.stringGE());
        return map;
    }

    public VarDec getVarDec(String name) {
        BasicNode i = hashMap.get(name);
        if (i instanceof VarDec) {
            return (VarDec) i;
        } else if (upperMap != null && i == null) {
            return upperMap.getVarDec(name);
        } else {
            throw new RuntimeException("Can't find the variable declare of " + name);
        }
    }

    public ClassDec getClassDec(String name) {
        BasicNode i = hashMap.get(name);
        if (i instanceof ClassDec) {
            return (ClassDec) i;
        } else if (upperMap != null && i == null) {
            return upperMap.getClassDec(name);
        } else if (i instanceof FuncDec && ((FuncDec) i).returnType == null) {
            return upperMap.getClassDec(name);
        } else {
            throw new RuntimeException("Can't find the class declare of " + name);
        }
    }

    public FuncDec getFuncDec(String name) {
        BasicNode i = hashMap.get(name);
        if (i instanceof FuncDec) {
            return (FuncDec) i;
        } else if (upperMap != null && i == null) {
            return upperMap.getFuncDec(name);
        } else {
            throw new RuntimeException("Can't find the function declare of " + name);
        }
    }

    public FuncDec getFuncDecTest(String name) {
        BasicNode i = hashMap.get(name);
        if (i instanceof FuncDec) {
            return (FuncDec) i;
        } else if (upperMap != null && i == null) {
            return upperMap.getFuncDecTest(name);
        } else {
            return null;
        }
    }

    public BasicType getClassType(String name) {
        return new ClassType(getClassDec(name));
    }

    public void addLink(String name, BasicNode node) {
        if (hashMap.containsKey(name)) {
            throw new RuntimeException(name + " has already exist");
        }
        hashMap.put(name, node);
    }

}