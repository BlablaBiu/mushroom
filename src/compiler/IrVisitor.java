package compiler;

import compiler.IR.IRInstrution.*;

/**
 * Created by blacko on 2017/5/26.
 */
public interface IrVisitor {
    void visit(BinaryOperation x);
    void visit(Call x);
    void visit(CmpAndJump x);
    void visit(EnterFunc x);
    void visit(IrInstruction x);
    void visit(Jump x);
    void visit(Label x);
    void visit(LeaveFunc x);
    void visit(Move x);
    void visit(UnaryOperation x);
    void visit(CallFunc x);
}
