package compiler.IR;

import compiler.AST.Declare.VarDec;
import compiler.IR.Operand.Address.GlobalAddress;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by blacko on 2017/5/26.
 */
public class IR {

    public List<Function> functions = new ArrayList<>();
    public List<VarDec> globalVariables = new ArrayList<>();
    public List<String> stringConstants = new ArrayList<>();
    public void add(Function tem) {
        functions.add(tem);
    }

    public GlobalAddress add(VarDec tem) {
        globalVariables.add(tem);
        return new GlobalAddress(tem.ID + "__", false);
    }

    public GlobalAddress add(String tem) {
        stringConstants.add(tem);
        return new GlobalAddress("string__" + Integer.toString(stringConstants.size() - 1), true);
    }

    public String toSring() {
        String tem = "";
        for(VarDec item : globalVariables) {
            tem += item.ID + " is [global Variable]\n\n";
        }

        for(int i = 0; i < stringConstants.size(); ++i) {
            tem += "string__" + Integer.toString(i) + ":\n" + stringConstants.get(i) + "\n\n";
        }

        for(Function item : functions) {
            tem += item.toString() + "\n\n";
        }

        return tem;
    }
}
