package compiler.IR.Operand;

/**
 * Created by blacko on 2017/5/24.
 */
public class ImmediateNumber extends Operand{
    public int value;
    public ImmediateNumber(int _value) {
        value = _value;
    }

    public String toString() {
        return Integer.toString(value);
    }
}
