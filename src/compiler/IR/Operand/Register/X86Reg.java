package compiler.IR.Operand.Register;

/**
 * Created by blacko on 2017/5/26.
 */
public class X86Reg extends Register {

    public static X86Reg rdi = new X86Reg("rdi", 0);
    public static X86Reg rsi = new X86Reg("rsi", 1);
    public static X86Reg rbx = new X86Reg("rbx", 2);
    public static X86Reg r8 = new X86Reg("r8", 3);
    public static X86Reg r9 = new X86Reg("r9", 4);
    public static X86Reg r10 = new X86Reg("r10", 5);
    public static X86Reg r11 = new X86Reg("r11", 6);
    public static X86Reg r12 = new X86Reg("r12", 7);
    public static X86Reg r13 = new X86Reg("r13", 8);
    public static X86Reg r14 = new X86Reg("r14", 9);
    public static X86Reg r15 = new X86Reg("r15", 10);
    public static X86Reg rdx = new X86Reg("rdx", 11);
    public static X86Reg rcx = new X86Reg("rcx", 12);
    public static X86Reg rax = new X86Reg("rax", 13);
    public static X86Reg rbp = new X86Reg("rbp", 14);
    public static X86Reg rsp = new X86Reg("rsp", 15);

    public static X86Reg getByRank(int i) {
        if(i == 0) return rdi;
        if(i == 1) return rsi;
        if(i == 2) return rbx;
        if(i == 3) return r8;
        if(i == 4) return r9;
        if(i == 5) return r10;
        if(i == 6) return r11;
        if(i == 7) return r12;
        if(i == 8) return r13;
        if(i == 9) return r14;
        if(i == 10) return r15;
        if(i == 11) return rdx;
        if(i == 12) return rcx;
        if(i == 13) return rax;
        if(i == 14) return rbp;
        if(i == 15) return rsp;
        throw new RuntimeException("???");
    }

    public static X86Reg ToX86(int i) {
        if(i == 0) return rdi;
        if(i == 1) return rsi;
        if(i == 2) return rbx;
        if(i == 3) return r8;
        if(i == 4) return r9;
        if(i == 5) return r10;
        if(i == 6) return r11;
        if(i == 7) return r12;
        if(i == 8) return r13;
        if(i == 9) return r14;
        if(i == 10) return r15;
        throw new RuntimeException("???");
    }

    public static X86Reg getCaller(int i) {
        if(i == 0) return r10;
        if(i == 1) return r11;
        if(i == 2) return rdi;
        if(i == 3) return rsi;
        if(i == 4) return r8;
        if(i == 5) return r9;
        throw new RuntimeException("????");
    }

    public static X86Reg getCallee(int i) {
        if(i == 0) return rbx;
        if(i == 1) return r12;
        if(i == 2) return r13;
        if(i == 3) return r14;
        if(i == 4) return r15;
        throw new RuntimeException("???");
    }


    public static X86Reg getParameterReg(int i) {
        if (i == 0) return rdi;
        if (i == 1) return rsi;
        if (i == 2) return rdx;
        if (i == 3) return rcx;
        if (i == 4) return r8;
        if (i == 5) return r9;
        throw new RuntimeException("??");
    }

    public X86Reg(String _name, int _index) {
        name = _name;
        index = _index;
    }

    @Override
    public String toString() {
        return name;
    }
}
