package compiler.IR.Operand.Register;

import compiler.IR.Operand.Operand;

/**
 * Created by blacko on 2017/5/24.
 */
public abstract class Register extends Operand {
    String name;
    public int index = 0;
}
