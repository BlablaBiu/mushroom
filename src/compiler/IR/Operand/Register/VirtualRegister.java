package compiler.IR.Operand.Register;

/**
 * Created by blacko on 2017/5/23.
 */
public class VirtualRegister extends Register {
    public VirtualRegister(int x) {
        name = "reg" + Integer.toString(x);
        index = x;
    }

    @Override
    public String toString() {
        if(index < 16) {
            return X86Reg.getByRank(index).toString();
        }
        return "reg" + Integer.toString(index);
    }
}
