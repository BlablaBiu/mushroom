package compiler.IR.Operand;

/**
 * Created by blacko on 2017/5/23.
 */
public abstract class Operand {
    public abstract String toString();
}
