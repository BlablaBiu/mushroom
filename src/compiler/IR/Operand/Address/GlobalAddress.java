package compiler.IR.Operand.Address;

/**
 * Created by blacko on 2017/5/24.
 */
public class GlobalAddress extends Address{
    String name;
    boolean isConstantString;
    public GlobalAddress(String _name, boolean tmp) {
        name = _name;
        isConstantString = tmp;
    }
    public String toString() {
        if(isConstantString) {
            return  name;
        } else {
            return "qword [" + name + "]";
        }
    }
}
