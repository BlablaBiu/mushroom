package compiler.IR.Operand.Address;

import compiler.IR.Operand.Register.Register;

/**
 * Created by blacko on 2017/5/24.
 */
public class MemoryAddress extends Address {
    public Register baseReg;
    public Register indexReg;
    public int scale;
    public int disp;
    public MemoryAddress(Register _baseReg, Register _indexReg, int _scale, int _disp) {
        baseReg = _baseReg;
        indexReg = _indexReg;
        scale = _scale;
        disp = _disp;
    }
    public String toString() {
        String tem = "qword [" ;
        if(baseReg != null) tem += baseReg.toString();
        if(indexReg != null) {
            tem += " + " + indexReg.toString() + " * " + Integer.toString(scale);
        }
        if(disp != 0) {
            if(disp < 0) {
                tem += " - " + Integer.toString(-disp);
            } else {
                tem += " + " + Integer.toString(disp);
            }
        }
        tem += "]";
        return tem;
    }
}
