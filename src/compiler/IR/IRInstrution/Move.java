package compiler.IR.IRInstrution;

import compiler.IR.Operand.Operand;
import compiler.IR.Operand.Register.Register;
import compiler.IrVisitor;

import java.util.Iterator;

/**
 * Created by blacko on 2017/5/23.
 */
public class Move extends IrInstruction {
    public Operand dest, source;
    public Move(Operand _dest, Operand _source) {
        dest = _dest;
        source = _source;
    }
    public String toString() {
        String tmp =  "\t"+ dest.toString() + " \t= " + source.toString();
        tmp += " " + getIn();
        return tmp;
    }
    public void visit(IrVisitor x) { x.visit(this); }
}
