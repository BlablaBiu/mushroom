package compiler.IR.IRInstrution;

import compiler.IR.Operand.Register.Register;
import compiler.IrVisitor;

import java.util.Iterator;

/**
 * Created by blacko on 2017/5/23.
 */
public class Jump extends IrInstruction {
    public Label label;
    public Jump(Label _label) {
        label = _label;
    }
    public String toString() {
        String tmp = "\t" + "jump " + label.name;
        tmp += " " + getIn();
        return  tmp;
    }

    @Override
    public void visit(IrVisitor x) {x.visit(this);}
}
