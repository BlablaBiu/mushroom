package compiler.IR.IRInstrution;

import compiler.IrVisitor;

/**
 * Created by blacko on 2017/5/24.
 */
public class LeaveFunc extends IrInstruction {
    public String toString() {
        String tmp = "\t" + "LeaveFunc";
        tmp += " " + getIn();
        return tmp;
    }
    public void visit(IrVisitor x) { x.visit(this); }
}
