package compiler.IR.IRInstrution;

import compiler.IR.Operand.Register.Register;
import compiler.IrVisitor;

import java.util.Iterator;

/**
 * Created by blacko on 2017/5/30.
 */
public class CallFunc extends IrInstruction{
    public String funcName;
    public int size;
    public CallFunc(String _funcName, int _size) {
        funcName = _funcName;
        size = _size;
    }

    public String toString() {
        String tmp = "\tcall " + funcName + "[size : " + Integer.toString(size) + "]";
        tmp += " " + getIn();
        return tmp;
    }
    public void visit(IrVisitor x) { x.visit(this); }
}
