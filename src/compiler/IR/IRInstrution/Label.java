package compiler.IR.IRInstrution;

import compiler.IR.Operand.Register.Register;
import compiler.IrVisitor;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by blacko on 2017/5/23.
 */
public class Label extends IrInstruction {
    public String name;
    public List<IrInstruction> prev = new ArrayList<>();
    static int cnt = 0;
    public Label(String _name) {
        name = _name;
    }
    public Label() {
        name = "_" + Integer.toString(cnt) + "_Label";
        ++cnt;
    }
    public String toString() {
        String tmp =  name + ":";
        tmp += " " + getIn();
        return  tmp;
    }
    public void visit(IrVisitor x) { x.visit(this); }
}
