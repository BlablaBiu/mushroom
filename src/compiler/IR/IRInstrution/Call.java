package compiler.IR.IRInstrution;

import compiler.IR.Operand.Address.MemoryAddress;
import compiler.IR.Operand.Operand;
import compiler.IR.Operand.Register.X86Reg;
import compiler.IrVisitor;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by blacko on 2017/5/23.
 */
public class Call extends IrInstruction {
    public int size;
    public List<IrInstruction> callIr = new ArrayList<>();
    public Call(String funcName, List<Operand> parameters, Operand dest) {
        size = parameters.size();
        for(int i = parameters.size(); i >= 1; --i) {
            Operand tmp = parameters.get(i - 1);
            if(i > 6) {
                callIr.add(new Move(new MemoryAddress(X86Reg.rsp, null, 0, (i - 7) * 8), tmp));
            } else {
                callIr.add(new Move(X86Reg.getParameterReg(i - 1), tmp));
            }
        }
        callIr.add(new CallFunc(funcName, parameters.size()));
        callIr.add(new Move(dest, X86Reg.rax));
    }
    public String toString() {
        String tem = "\n";
        for(IrInstruction item : callIr) tem += "\t" + item.toString() + "\n";
        return tem;
    }
    public void visit(IrVisitor x) { x.visit(this); }
}
