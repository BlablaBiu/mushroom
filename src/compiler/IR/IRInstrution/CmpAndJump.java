package compiler.IR.IRInstrution;

import compiler.IR.Operand.Operand;
import compiler.IR.Operand.Register.Register;
import compiler.IrVisitor;

import java.util.Iterator;

/**
 * Created by blacko on 2017/5/24.
 */
public class CmpAndJump extends IrInstruction {
    public Operand lhs, rhs;
    public String op;
    public Label target;
    public CmpAndJump(Operand _lhs, Operand _rhs, String _op, Label _target) {
        lhs = _lhs;
        rhs = _rhs;
        op = _op;
        target = _target;
    }
    public String toString() {
        String tmp = "\t" + "jump " + target.name + " if " + lhs.toString() + " " + op + " " + rhs.toString();tmp += "[";
        tmp += " " + getIn();
        return tmp;
    }
    public void visit(IrVisitor x) { x.visit(this); }
}
