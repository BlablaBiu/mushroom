package compiler.IR.IRInstrution;

import compiler.IR.Operand.Register.Register;
import compiler.IrVisitor;

import java.util.Iterator;

/**
 * Created by blacko on 2017/5/24.
 */
public class EnterFunc extends IrInstruction {
    public String toString() {
        String tmp = "\t" + "EnterFunc";
        tmp += " " + getIn();
        return  tmp;
    }
    public void visit(IrVisitor x) { x.visit(this); }
}
