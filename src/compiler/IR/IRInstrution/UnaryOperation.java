package compiler.IR.IRInstrution;

import compiler.IR.Operand.Operand;
import compiler.IR.Operand.Register.Register;
import compiler.IrVisitor;

import java.util.Iterator;

/**
 * Created by blacko on 2017/5/23.
 */
public class UnaryOperation extends IrInstruction {
    public Operand dest;
    public String op;
    public UnaryOperation(Operand _dest, String _op) {
        dest = _dest;
        op = _op;
    }
    public String toString() {
        String tmp = "\t" + op + " " + dest.toString();
        tmp += " " + getIn();
        return  tmp;
    }
    public void visit(IrVisitor x) { x.visit(this); }
}
