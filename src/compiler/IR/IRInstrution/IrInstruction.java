package compiler.IR.IRInstrution;

import compiler.IR.Operand.Register.Register;
import compiler.IrVisitor;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by blacko on 2017/5/17.
 */
public abstract class IrInstruction {
    public abstract String toString();
    public abstract void visit(IrVisitor x);
    public Set<Register> in = new HashSet<>();
    public Register def = null;
    public IrInstruction next;
    String getIn() {
        String tmp = "[";
        for(Iterator<Register> it = in.iterator(); it.hasNext();)
        {
            tmp += it.next().toString() + " | ";
        }
        tmp += "]";
        tmp += "[";
        if(def != null) tmp += def.toString();
        tmp += "]";
        return tmp;
    }
}
