package compiler.IR.IRInstrution;

import compiler.IR.Operand.Operand;
import compiler.IR.Operand.Register.Register;
import compiler.IrVisitor;

import java.util.Iterator;

/**
 * Created by blacko on 2017/5/23.
 */
public class BinaryOperation extends IrInstruction {
    public Operand dest, lhs, rhs;
    public String op;
    public BinaryOperation(Operand _dest, String _op, Operand _lhs, Operand _rhs) {
        dest = _dest;
        op = _op;
        lhs = _lhs;
        rhs = _rhs;
    }
    public String toString() {
        String tmp =  "\t" + dest.toString() + " \t= " + lhs.toString() + " " + op + " " + rhs.toString();
        tmp += " " + getIn();
        return tmp;
    }
    public void visit(IrVisitor x) { x.visit(this); }
}
