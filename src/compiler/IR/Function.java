package compiler.IR;

import compiler.IR.IRInstrution.IrInstruction;
import compiler.IR.IRInstrution.Label;
import compiler.IR.Operand.Register.Register;
import compiler.IR.Operand.Register.VirtualRegister;
import compiler.IR.Operand.Register.X86Reg;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by blacko on 2017/5/25.
 */
public class Function {
    public String name;
    public List<IrInstruction> insList =  new ArrayList<>();
    public List<Register> regList = new ArrayList<>();
    public int numOfVirtualReg = 16;
    public int MaxReg = 0;
    public int maxSize = 0;
    public boolean optim = false;
    public boolean[] useRegTag;
    public Function(String _name) {
        name = _name;
        for(int i = 0; i < 16; ++i) regList.add(X86Reg.getByRank(i));
    }

    public void addIns(IrInstruction x) {
        insList.add(x);
    }

    public VirtualRegister getReg() {
//        return new MemoryAddress(X86Reg.getStackHeadReg(), null, 0, -8 * (++numOfVirtualReg));
        VirtualRegister tmp = new VirtualRegister(numOfVirtualReg++);
        regList.add(tmp);
        return tmp;
    }

    public String toString() {
        String tem = name + "[you need to fix]:\n";
//        tem += "\t\tENTER FUNCTION\n";
        for(IrInstruction item : insList) tem += item.toString() + '\n';
//        tem += "\t\tLEAVE FUNCTION\n";
        return tem;
    }

}
