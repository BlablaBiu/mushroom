package compiler;

import compiler.AST.Basic.*;
import compiler.AST.*;
import compiler.AST.Declare.*;
import compiler.AST.Statment.*;
import compiler.AST.Expression.*;
import compiler.AST.Type.*;
import compiler.Paser.MushroomBaseVisitor;
import compiler.Paser.MushroomParser;
import org.antlr.v4.runtime.tree.ParseTree;

import java.util.List;
import java.util.Stack;

class ASTBuilder extends MushroomBaseVisitor<BasicNode> {
    private LinkMap currentMap;
    private Stack<BasicStat> upperStats = new Stack<>();
    private FuncDec upperFunc;
    private ClassDec upperClass;
    private Program myProgram;
    private void checkMainFunc(FuncDec x) {
        if(!(x != null && x.returnType instanceof IntType && x.parameters.isEmpty())) {
            throw new RuntimeException("Main function semantic error");
        }
    }

    private void checkMemberFunction(FuncDec x) {
        if(x.returnType != null) return ;
        if(x.upperClass == null) {
            throw new RuntimeException("Function "  + x.ID + "doesn't have return type");
        }
        if(!x.ID.equals(upperClass.ID + "_" + upperClass.ID)) {
            throw new RuntimeException("Function "  + x.ID + "isn't the initial function of class " + x.upperClass.ID);
        }
        if(x.parameters.size() != 1) {
            throw new RuntimeException("Initial Function "  + x.ID + "shouldn't have parameters");
        }
        upperClass.startFunc = x;
    }

    private boolean checkParameters(List<VarDec> lx, List<BasicExpr> rx) {
        if (lx.size() != rx.size()) return false;
        for (int i = 0; i < lx.size(); ++i) {
            if(!lx.get(i).type.accept(rx.get(i).type)) {
                return false;
            }
        }
        return true;
    }
    private Program preVisitProgram(MushroomParser.ProgramContext ctx) {
        Program item = myProgram = new Program();
        currentMap = item.map = LinkMap.getGlobalMap();
        for (MushroomParser.ClassDecContext x : ctx.classDec()) {
            currentMap.addLink(x.ID().getText(), new ClassDec());
        }
        for (MushroomParser.ClassDecContext x : ctx.classDec()) {
            ClassDec classItem = currentMap.getClassDec(x.ID().getText());
            upperClass = classItem;
            currentMap = classItem.map = new LinkMap(currentMap);
            classItem.ID = x.ID().getText();
            for (MushroomParser.ToVarDecContext xx : x.toVarDec()) {
                classItem.decs.add((BasicDec) visit(xx.varDec()));
                ((VarDec)classItem.decs.get(classItem.decs.size() - 1)).upperClass = classItem;
            }
            for (MushroomParser.FuncDecContext xx : x.funcDec()) preVisitFunc(xx);
            currentMap = currentMap.upperMap;
            upperClass = null;
        }
        for (MushroomParser.FuncDecContext x : ctx.funcDec()) preVisitFunc(x);
        return item;
    }

    private FuncDec preVisitFunc(MushroomParser.FuncDecContext ctx) {
        FuncDec item = new FuncDec();
        item.stat = new BlockStat();
        if(ctx.ID().getText().equals("this")) {
            throw new RuntimeException("function name can't be this");
        }
        item.stat.map = currentMap = new LinkMap(currentMap);
        if (upperClass != null) {
            VarDec tmp = new VarDec();
            tmp.ID = "this";
            tmp.type = new ClassType(upperClass);
            currentMap.addLink(tmp.ID, tmp);
            item.parameters.add(tmp);
            item.ID = upperClass.ID + "_" + ctx.ID().getText();
        } else {
            item.ID = ctx.ID().getText();
        }
        for (MushroomParser.VarDecContext x : ctx.varDec()) {
            item.parameters.add((VarDec) visit(x));
        }
        for(VarDec x: item.parameters) x.isParameter = true;
        if(ctx.type() != null) item.returnType = (BasicType) visit(ctx.type());
        if(item.returnType instanceof NullType) {
            throw new RuntimeException("null can't be return types");
        }
        item.upperClass = upperClass;
        currentMap = currentMap.upperMap;
        myProgram.map.addLink(item.ID, item);
        return item;
    }

    @Override public BasicNode visitProgram(MushroomParser.ProgramContext ctx) {
        Program item = preVisitProgram(ctx);

        for(ParseTree x : ctx.children) {
            BasicDec tem = (BasicDec)visit(x);
            if(tem == null) continue; // null continue
            item.decs.add(tem);
        }

        checkMainFunc(item.map.getFuncDec("main"));
        return item;
    }

    @Override public BasicNode visitClassDec(MushroomParser.ClassDecContext ctx) {
        ClassDec item = currentMap.getClassDec(ctx.ID().getText());
        currentMap = item.map;
        upperClass = item;
        for(ParseTree x : ctx.funcDec()) {
            myProgram.decs.add((BasicDec) visit(x));
        }
        upperClass = null;
        currentMap = currentMap.upperMap;
        return item;
    }

    @Override public BasicNode visitFuncDec(MushroomParser.FuncDecContext ctx) {
        FuncDec item;
        if(upperClass != null) {
            item = currentMap.getFuncDec(upperClass.ID + "_" + ctx.ID().getText());
        } else {
            item = currentMap.getFuncDec(ctx.ID().getText());
        }
        upperFunc = item;
        item.isDec = false;
        visit(ctx.blockStat());
        upperFunc = null;
        checkMemberFunction(item);
        return item;
    }

    @Override public BasicNode visitToVarDec(MushroomParser.ToVarDecContext ctx) { return visit(ctx.varDec()); }

    @Override public BasicNode visitVarDec(MushroomParser.VarDecContext ctx) {
        VarDec item = new VarDec();
        item.type = (BasicType) visit(ctx.type());
        if(item.type instanceof VoidType || item.type instanceof NullType) {
            throw new RuntimeException("variable can't be this type");
        }
        item.ID = ctx.ID().getText();
//        item.upperClass = upperClass;
        if (item.ID.equals("this")) {
            throw new RuntimeException("variable can't be this");
        }
        if (ctx.expr() != null) {
            item.expr = (BasicExpr) visit(ctx.expr());
            if (!item.type.accept(item.expr.type)) {
                throw new RuntimeException("Initial Function of " + item.ID + " error");
            }
        }
        currentMap.addLink(item.ID, item);
        return item;
    }

    @Override public BasicNode visitExprStat(MushroomParser.ExprStatContext ctx) {
        ExprStat item = new ExprStat();
        if (ctx.expr() != null)  item.expr = (BasicExpr) visit(ctx.expr());
        return item;
    }

    @Override public BasicNode visitIfStat(MushroomParser.IfStatContext ctx) {
        IfStat item = new IfStat();
        item.expr = (BasicExpr) visit(ctx.expr());
        if(!(item.expr.type instanceof BoolType)) {
            throw new RuntimeException();
        }
        item.trueStat = (BasicStat) visit(ctx.stat(0));
        if(ctx.stat(1) != null) item.falseStat = (BasicStat) visit(ctx.stat(1));
        return item;
    }

    @Override public BasicNode visitWhileStat(MushroomParser.WhileStatContext ctx) {
        ForStat item = new ForStat();
        upperStats.push(item);
        item.init = null;
        item.step = null;
        item.cond = (BasicExpr) visit(ctx.expr());
        if(!(item.cond.type instanceof BoolType)) {
            throw new RuntimeException();
        }
        item.stat = (BasicStat) visit(ctx.stat());
        upperStats.pop();
        return item;
    }

    @Override public BasicNode visitForStat(MushroomParser.ForStatContext ctx) {
        ForStat item = new ForStat();
        upperStats.push(item);
        if(ctx.init != null) item.init = (BasicExpr) visit(ctx.init);
        if(ctx.cond != null) {
            item.cond = (BasicExpr) visit(ctx.cond);
            if (!(item.cond.type instanceof BoolType)) {
                throw new RuntimeException();
            }
        }
        if(ctx.step != null) item.step = (BasicExpr) visit(ctx.step);
        item.stat = (BasicStat) visit(ctx.stat());
        upperStats.pop();
        return item;
    }

    @Override public BasicNode visitContinueStat(MushroomParser.ContinueStatContext ctx) {
        ContinueStat item = new ContinueStat();
        if(upperStats.empty()) {
            throw new RuntimeException("continue without iteration statement");
        } else {
            item.upperStat = upperStats.peek();
        }
        return item;
    }

    @Override public BasicNode visitBreakStat(MushroomParser.BreakStatContext ctx) {
        BreakStat item = new BreakStat();
        if(upperStats.empty()) {
            throw new RuntimeException("break without iteration statement");
        } else {
            item.upperStat = upperStats.peek();
        }
        return item;
    }

    @Override public BasicNode visitReturnStat(MushroomParser.ReturnStatContext ctx) {
        ReturnStat item = new ReturnStat();
        if(upperFunc == null) {
            throw new RuntimeException("return without function");
        } else {
            item.upperFunc = upperFunc;
        }
        if (ctx.expr() != null) {
            item.expr = (BasicExpr) visit(ctx.expr());
            if (!item.upperFunc.returnType.accept(item.expr.type)) {
                throw new RuntimeException();
            }
        } else {
            if(!(item.upperFunc.returnType instanceof VoidType || item.upperFunc.returnType == null)) {
                throw new RuntimeException();
            }
        }
        return item;
    }

    @Override public BasicNode visitToBlockStat(MushroomParser.ToBlockStatContext ctx) {
        return visit(ctx.blockStat());
    }

    @Override public BasicNode visitBlockStat(MushroomParser.BlockStatContext ctx) {
        BlockStat item;
        if(!upperFunc.isDec) {
            item = upperFunc.stat;
            currentMap = item.map;
            upperFunc.isDec = true;
        } else {
            item = new BlockStat();
            item.map = currentMap = new LinkMap(currentMap);
        }
        for(ParseTree x : ctx.children) {
            BasicNode tem = visit(x);
            if(tem == null) continue; // null continue
            item.VarDecAndStat.add(tem);
        }
        currentMap = currentMap.upperMap;
        return item;
    }

    @Override public BasicNode visitAssignmentExpr(MushroomParser.AssignmentExprContext ctx) {
        AssignmentExpr item = new AssignmentExpr();
        item.lExpr = (BasicExpr) visit(ctx.expr(0));
        item.rExpr = (BasicExpr) visit(ctx.expr(1));
        if(!(item.lExpr.type.accept(item.rExpr.type)) || !item.lExpr.isLeftValue) {
            throw new RuntimeException(item.toString());
        }
        item.type = new VoidType();
        item.isLeftValue = false;
        return item;
    }

    @Override public BasicNode visitFuncExpr(MushroomParser.FuncExprContext ctx) {
        FuncExpr item = new FuncExpr();
        if(upperClass != null) {
            item.funcDec = currentMap.getFuncDecTest(upperClass.ID + "_" + ctx.ID().getText());
        }
        if(item.funcDec == null) {
            item.funcDec = currentMap.getFuncDec(ctx.ID().getText());
        }
        if(item.funcDec.upperClass != null) {
            VarExpr tmp = new VarExpr();
            tmp.varDec = currentMap.getVarDec("this");
            tmp.type = tmp.varDec.type;
            tmp.isLeftValue = true;
            item.parameters.add(tmp);
        }
        for(ParseTree x: ctx.expr()) {
            item.parameters.add((BasicExpr) visit(x));
        }
        if (!checkParameters(item.funcDec.parameters, item.parameters)) {
            throw new RuntimeException();
        }
        item.type = item.funcDec.returnType;
        item.isLeftValue = false;
        return item;
    }

    @Override public BasicNode visitMemberFuncExpr(MushroomParser.MemberFuncExprContext ctx) {
        FuncExpr item = new FuncExpr();
        BasicExpr callExpr = ((BasicExpr) visit(ctx.expr(0)));
        if(callExpr.type instanceof ClassType) {
            item.funcDec = currentMap.getFuncDec(((ClassType) callExpr.type).classDec.ID + "_" + ctx.ID().getText());
        } else if(callExpr.type instanceof StringType) {
            item.funcDec = currentMap.getFuncDec("String_" + ctx.ID().getText());
        } else if(callExpr.type instanceof ArrayType) {
            item.funcDec = currentMap.getFuncDec("Array_" + ctx.ID().getText());
        } else {
            throw new RuntimeException();
        }
        item.parameters.add(callExpr);
        for(ParseTree x: ctx.expr().subList(1, ctx.expr().size())) {
            item.parameters.add((BasicExpr) visit(x));
        }
        if (!checkParameters(item.funcDec.parameters, item.parameters)) {
            throw new RuntimeException(item.toString());
        }
        item.type = item.funcDec.returnType;
        item.isLeftValue = false;
        return item;
    }

    @Override public BasicNode visitMemberExpr(MushroomParser.MemberExprContext ctx) {
        MemberExpr item = new MemberExpr();
        item.expr = (BasicExpr) visit(ctx.expr());
        item.ID = ctx.ID().getText();
        if(!(item.expr.type instanceof ClassType)) {
            throw new RuntimeException();
        } else {
            item.varDec = ((ClassType)item.expr.type).classDec.map.getVarDec(item.ID);
        }
        item.type = item.varDec.type;
        item.isLeftValue = true;
        return item;
    }

    @Override public BasicNode visitNewNonArrayExpr(MushroomParser.NewNonArrayExprContext ctx) {
        NewExpr item = new NewExpr();
        item.type = (BasicType) visit(ctx.nonArrayType());
        item.isLeftValue = false;
        return item;
    }

    @Override public BasicNode visitArrayExpr(MushroomParser.ArrayExprContext ctx) {
        ArrayExpr item = new ArrayExpr();
        item.outExpr = (BasicExpr) visit(ctx.expr(0));
        item.inExpr = (BasicExpr) visit(ctx.expr(1));
        if(!(item.outExpr.type instanceof ArrayType)) {
            throw new RuntimeException();
        }
        if(!(item.inExpr.type instanceof IntType)) {
            throw new RuntimeException();
        }
        item.type = ((ArrayType) item.outExpr.type).memberType();
        item.isLeftValue = true;
        return item;
    }
    FuncExpr BinaryExprToFuncExpr(BasicExpr lhs, BasicExpr rhs, FuncDec dec) {
        FuncExpr item = new FuncExpr();
        item.funcDec = dec;
        item.parameters.add(lhs);
        item.parameters.add(rhs);
        item.type = dec.returnType;
        item.isLeftValue = false;
        return item;
    }

    UnaryExpr ExprToFalseExpr(BasicExpr tmp) {
        UnaryExpr item = new UnaryExpr();
        item.expr = tmp;
        item.op = "!";
        item.type = new BoolType();
        item.isLeftValue = false;
        return item;
    }
    BinaryExpr makeBinary(BasicExpr lhs, BasicExpr rhs, String op, BasicType returnType) {
        BinaryExpr item = new BinaryExpr();
        item.lExpr = lhs;
        item.rExpr = rhs;
        item.op = op;
        item.type = returnType;
        item.isLeftValue = false;
        return item;
    }
    @Override public BasicNode visitBinaryExpr(MushroomParser.BinaryExprContext ctx) {
        BasicExpr lhs = (BasicExpr) visit(ctx.expr(0));
        BasicExpr rhs = (BasicExpr) visit(ctx.expr(1));
        String op = ctx.op.getText();
        boolean allInt = (lhs.type instanceof IntType && rhs.type instanceof IntType);
        boolean allString = (lhs.type instanceof StringType && rhs.type instanceof StringType);
        boolean allBool = (lhs.type instanceof BoolType && rhs.type instanceof BoolType);
        if(lhs instanceof ConstantExpr && rhs instanceof ConstantExpr) {
            if(allInt) {
                int vL = ((ConstantExpr) lhs).valInt;
                int vR = ((ConstantExpr) rhs).valInt;
                switch (op) {
                    case "==": return new ConstantExpr(vL == vR);
                    case "!=": return new ConstantExpr(vL != vR);
                    case ">=": return new ConstantExpr(vL >= vR);
                    case "<=": return new ConstantExpr(vL <= vR);
                    case ">": return new ConstantExpr(vL > vR);
                    case "<": return new ConstantExpr(vL < vR);
                    case "+": return new ConstantExpr(vL + vR);
                    case "-": return new ConstantExpr(vL - vR);
                    case "*": return new ConstantExpr(vL * vR);
                    case "/": return new ConstantExpr(vL / vR);
                    case "%": return new ConstantExpr(vL % vR);
                    case "&": return new ConstantExpr(vL & vR);
                    case "^": return new ConstantExpr(vL ^ vR);
                    case "|": return new ConstantExpr(vL | vR);
                    case "<<": return new ConstantExpr(vL << vR);
                    case ">>": return new ConstantExpr(vL >> vR);
                }
            }
            if(allBool) {
                boolean vL = ((ConstantExpr) lhs).valBool;
                boolean vR = ((ConstantExpr) rhs).valBool;
                switch (op) {
                    case "&&":return new ConstantExpr(vL && vR);
                    case "||":return new ConstantExpr(vL || vR);
                }
            }
        }
        if(allString) {
            switch (op) {
                case "+":
                    return BinaryExprToFuncExpr(lhs, rhs, currentMap.getFuncDec("String_add"));
                case "==":
                    return BinaryExprToFuncExpr(lhs, rhs, currentMap.getFuncDec("String_eq"));
                case "!=":
                    return BinaryExprToFuncExpr(lhs, rhs, currentMap.getFuncDec("String_ne"));
                case ">":
                    return BinaryExprToFuncExpr(lhs, rhs, currentMap.getFuncDec("String_gt"));
                case "<":
                    return BinaryExprToFuncExpr(lhs, rhs, currentMap.getFuncDec("String_lt"));
                case ">=":
                    return BinaryExprToFuncExpr(lhs, rhs, currentMap.getFuncDec("String_ge"));
                case "<=":
                    return BinaryExprToFuncExpr(lhs, rhs, currentMap.getFuncDec("String_le"));
                default:
                    throw new RuntimeException("[something wrong]");
            }
        }
        switch (op) {
            case "==":
            case "!=":
                if(lhs.type.accept(rhs.type) || rhs.type.accept(lhs.type)) {
                    return makeBinary(lhs, rhs, op, new BoolType());
                } else {
                    throw new RuntimeException();
                }
            case ">=":
            case "<=":
            case ">":
            case "<":
                if (allInt) {
                    return makeBinary(lhs, rhs, op, new BoolType());
                } else {
                    throw new RuntimeException();
                }
            case "+":
            case "-":
            case "*":
            case "/":
            case "%":
            case "&":
            case "^":
            case "|":
            case "<<":
            case ">>":
                if (allInt) {
                    return makeBinary(lhs, rhs, op, new IntType());
                } else {
                    throw new RuntimeException();
                }
            case "&&":
            case "||":
                if (allBool) {
                    return makeBinary(lhs, rhs, op, new BoolType());
                } else {
                    throw new RuntimeException();
                }
            default:
                throw new RuntimeException("[something wrong]");
        }
    }

    @Override public BasicNode visitUnaryExpr(MushroomParser.UnaryExprContext ctx) {
        UnaryExpr item = new UnaryExpr();
        item.op = ctx.op.getText();
        item.expr = (BasicExpr) visit(ctx.expr());
        switch (item.op) {
            case "+" :
            case "-" :
            case "~" :
            case "++" :
            case "--" :
                if(!(item.expr.type instanceof IntType)) {
                    throw new RuntimeException();
                }
                item.type = new IntType();
                break;
            case "!" :
                if(!(item.expr.type instanceof BoolType)) {
                    throw new RuntimeException();
                }
                item.type = new BoolType();
                break;
            default :
                throw new RuntimeException();
        }
        item.isLeftValue = false;
        return item;
    }

    @Override public BasicNode visitPostfixExpr(MushroomParser.PostfixExprContext ctx) {
        PostfixExpr item = new PostfixExpr();
        item.op = ctx.op.getText();
        item.expr = (BasicExpr) visit(ctx.expr());
        if(!(item.expr.type instanceof IntType) || !item.expr.isLeftValue) {
            throw new RuntimeException();
        }
        item.type = item.expr.type;
        item.isLeftValue = false;
        return item;
    }

    @Override public BasicNode visitNewArrayExpr(MushroomParser.NewArrayExprContext ctx) {
        NewArrayExpr item = new NewArrayExpr();
        item.bodyType = (BasicType) visit(ctx.nonArrayType());
        int tmp = ctx.getText().length() - ctx.nonArrayType().getText().length() - 3;
        for(ParseTree x : ctx.expr()) {
            item.expDim.add((BasicExpr) visit(x));
            tmp -= x.getText().length();
        }
        tmp /= 2;
        item.emptyDim = tmp - item.expDim.size();
        for(BasicExpr dimItem : item.expDim) {
            if(!(dimItem.type instanceof IntType)) {
                throw new RuntimeException("Array Size should be Int");
            }
        }
        item.type = new ArrayType(item.bodyType, item.emptyDim + item.expDim.size());
        item.isLeftValue = false;
        return item;
    }

    @Override public BasicNode visitConstantExpr(MushroomParser.ConstantExprContext ctx) {
        ConstantExpr item = ((ConstantExpr)visit(ctx.constant()));
        item.isLeftValue = false;
        return item;
    }

    @Override public BasicNode visitError(MushroomParser.ErrorContext ctx) {
        throw new RuntimeException("visit Error Parse");
    }

    @Override public BasicNode visitVarExpr(MushroomParser.VarExprContext ctx) {
        VarExpr tmpItem = new VarExpr();
        tmpItem.varDec = currentMap.getVarDec(ctx.getText());
        if(tmpItem.varDec.upperClass != null) {
            VarExpr thisExpr = new VarExpr();
            thisExpr.varDec = currentMap.getVarDec("this");
            thisExpr.type = thisExpr.varDec.type;
            thisExpr.isLeftValue = true;

            MemberExpr item = new MemberExpr();
            item.expr = thisExpr;
            item.ID = ctx.ID().getText();
            item.varDec = tmpItem.varDec;
            item.type = item.varDec.type;
            item.isLeftValue = true;

            return item;
        } else {
            tmpItem.type = tmpItem.varDec.type;
            tmpItem.isLeftValue = true;
            return tmpItem;
        }
    }

    @Override public BasicNode visitSubExpr(MushroomParser.SubExprContext ctx) { return visit(ctx.expr()); }


    @Override public BasicNode visitToNonArrayType(MushroomParser.ToNonArrayTypeContext ctx) { return visit(ctx.nonArrayType()); }

    @Override public BasicNode visitArrayType(MushroomParser.ArrayTypeContext ctx) {
        int tmp = (ctx.getText().length() - ctx.nonArrayType().getText().length()) / 2;
        return new ArrayType((BasicType) visit(ctx.nonArrayType()),  tmp);
    }

    @Override public BasicNode visitVoidType(MushroomParser.VoidTypeContext ctx) { return new VoidType(); }
    @Override public BasicNode visitIntType(MushroomParser.IntTypeContext ctx) { return new IntType(); }
    @Override public BasicNode visitBoolType(MushroomParser.BoolTypeContext ctx) { return new BoolType(); }
    @Override public BasicNode visitStrType(MushroomParser.StrTypeContext ctx) { return new StringType(); }
    @Override public BasicNode visitClassType(MushroomParser.ClassTypeContext ctx) { return currentMap.getClassType(ctx.getText()); }

    @Override public BasicNode visitBoolConstant(MushroomParser.BoolConstantContext ctx) { return new ConstantExpr(Boolean.valueOf(ctx.getText())); }
    @Override public BasicNode visitNullConstant(MushroomParser.NullConstantContext ctx) { return new ConstantExpr(); }
    @Override public BasicNode visitIntConstant(MushroomParser.IntConstantContext ctx) { return new ConstantExpr(Integer.valueOf(ctx.getText())); }
    @Override public BasicNode visitStrConstant(MushroomParser.StrConstantContext ctx) {
        String A = ctx.getText().substring(1, ctx.getText().length() - 1);
        String B = "";
        for(int i = 0; i < A.length(); ++i) {
            if(A.charAt(i) == '\\') {
                ++i;
                switch (A.charAt(i)) {
                    case 'n': B += '\n'; break;
                    case '\\': B += '\\'; break;
                    case 't': B += '\t'; break;
                    case 'r': B += '\r'; break;
                    case '\"': B += '\"'; break;
                    default:
                        throw new RuntimeException("???");
                }
            } else {
                B += A.charAt(i);
            }
        }
        return new ConstantExpr(B);
    }
}
